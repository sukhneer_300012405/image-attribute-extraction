from pyhive import presto
import time
import os
import re
import sys
import traceback
import datetime
import csv, codecs, cStringIO

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        try:
            self.writer.writerow([s.replace("\n", " ").encode("utf-8") for s in row])
        except AttributeError as e:
            None
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


db_user = "hadoop"
db_pwd = ""
db_host = "rapid-analytics.mynt.myntra.com"
db_port = 8889
db_connections = {}

def u_encode(input):                                                                                  
    if isinstance(input, dict):                                                                       
        return {u_encode(key):u_encode(value) for key,value in input.iteritems()}                     
    elif isinstance(input, list):                                                                     
        return [u_encode(element) for element in input]                                               
    elif isinstance(input, unicode):                                                                  
        return input.encode('utf-8')  
    else:                                                                                             
        return input

def execute_query_and_write_output(str, source='output'):
    conn = get_pg_conn()
    cursor = conn.cursor()
    cursor.execute(str)
    f = open('/scratch4/text_attrib/data/all_source/dresses/%s.csv' % source, 'w')
    filename='/scratch4/text_attrib/data/all_source/dresses/%s.csv' % source
    FILE=open(filename,"w");
    output=UnicodeWriter(FILE)

    for row in cursor: 
        output.writerow(u_encode(row))
    cursor.close()
    FILE.close()

def get_pg_conn():
    pid = str(os.getpid())
    conn = None
    # get the database connection for this PID
    try:
        conn = db_connections[pid]
    except KeyError:
        pass

    if conn == None:
        try:
            conn = presto.connect(host=db_host, port=db_port, username=db_user)
        except Exception as e:
            write(e)
            write('Unable to connect to Cluster Endpoint')
            cleanup()
            sys.exit(ERROR)
        db_connections[pid] = conn
    return conn




if __name__ == "__main__":
    
    sql_query = '''
        select 
json_extract_scalar(product_attributes, '$["id"]') as style_id, 
json_extract_scalar(product_attributes, '$["data"]["name"]') as name,
json_extract_scalar(product_attributes, '$["data"]["description"]') as description,
json_extract_scalar(product_attributes, '$["data"]["attributes"]["ECMC_PROD_CE3_STYLE_3"]') as articleType
from rapid.topman_merge_data  
        '''
    sql_query1 = '''
    select
    style_id,
    description,
    article_type,
    product_url
    from
    (
    select distinct
    json_extract_scalar(product_attributes, '$.id') as style_id,
    json_extract_scalar(product_attributes, '$.original_data.pdp_data.description') as description,
    json_extract_scalar(product_attributes, '$.original_data.pdp_data.category') as article_type,
    'http://www.asos.com/' || json_extract_scalar(product_attributes, '$.original_data.list_data.url') as product_url
    from rapid.asos_merged_data)
    '''
    
    sql_query2 = '''
    select 
    json_extract_scalar(product_attributes, '$.id') as product_id,
    json_extract_scalar(product_attributes, '$.data.gender') as gender,
    json_extract_scalar(product_attributes, '$.data.category') as article_type,
    json_extract_scalar(product_attributes, '$.data.name') as title,
    json_extract_scalar(product_attributes, '$.data.description') as description,
    json_extract_scalar(product_attributes, '$.data.color[0]') as color,
    json_extract_scalar(product_attributes, '$.data.url') as product_url
    from 
    fifa.forever21_staging 
    '''


    execute_query_and_write_output(sql_query1, "asos_dress")
