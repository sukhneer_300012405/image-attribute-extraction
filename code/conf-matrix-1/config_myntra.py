__author__ = '8372'

import os

evaluation_for = "test"
article_type= "tops"
attribute = "sleeve_length"
architecture_folder_name= "res34_0.005"
base_folder = '/rapid_data/myntra/myntra_data/'

REPO_DIRNAME = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../..')
MODEL_PATH_DICT = dict(description='Fine-tuned ImageNet model for back pose',
                       pathToStoreOutput='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/evaluation/'+evaluation_for,
                       mapFilePath='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/'+article_type+'_'+attribute+'_label_map.txt', 
                       modelPath='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/ckpt_log_aug_data/'+architecture_folder_name+'/*',
testFilePath='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/'+article_type+'_'+attribute+'_'+evaluation_for+'.txt',
predictionStorePath='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/evaluation/'+evaluation_for+'/predictions.txt',
usePreviousPredictions = True,
confidenceThreshholdList = [80,70,60,50],
attribute_list = ['neck','sleeve_length'])
