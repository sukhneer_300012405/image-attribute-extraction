
import time
import logging
import optparse
import numpy as np
import pandas as pd
from config import *
import exifutil
import caffe


# TO DO: Check if all file exists, else don't let the model through

class ModelClassifier(object):
    def __init__(self, model_def_file, pretrained_model_file, mean_file,
                 raw_scale, class_labels_file, image_dim, gpu_mode):
        logging.info('Loading net and associated files...')
        if gpu_mode:
            caffe.set_mode_gpu()
            caffe.set_device(1)
        else:
            caffe.set_mode_cpu()
        #mean1 = np.load(mean_file)
        #mean2 = imresize(mean1,(224,224)).transpose((2,0,1))
        #mean3 = mean2[:,:,::-1]
        #print mean2.shape
        center = np.array((256, 256)) / 2.0
        crop_dims = np.asanyarray([224, 224])
        crop = np.tile(center, (1, 2))[0] + np.concatenate([
                -crop_dims / 2.0,
                crop_dims / 2.0
            ])
        _mean = np.load(mean_file)[:, crop[0]:crop[2], crop[1]:crop[3]][:,:,::-1]
        self.net = caffe.Classifier(
            model_def_file, pretrained_model_file,
            image_dims=(image_dim, image_dim), raw_scale=raw_scale,
            mean=_mean, channel_swap=(2, 1, 0)
        )

        with open(class_labels_file) as f:
            labels_df = pd.DataFrame([
                                         {
                                             'id': l.strip().split(' ')[0][1:],
                                             # The [1:] to get rid of 'n' in labels of the format n<label_id>
                                             'name': ' '.join(l.strip().split(' ')[1:]).split(',')[0]
                                         }
                                         for l in f.readlines()
                                         ])
            labels_df = labels_df.convert_objects(convert_numeric=True)
        self.labels = labels_df.sort('id')['name'].values

    def classify_image(self, image):
        try:
            starttime = time.time()
            scores = self.net.predict([image], oversample=False).flatten()
            endtime = time.time()

            indices = (-scores).argsort()[:1]
            predictions = self.labels[indices]

            # In addition to the prediction text, we will also produce
            # the length for the progress bar visualization.
            meta = [
                (p, '%.5f' % scores[i])
                for i, p in zip(indices, predictions)
                ]
            logging.info('result: %s', str(meta))
#            print (endtime-starttime)

            return True, meta, '%.3f' % (endtime - starttime)

        except Exception as err:
            logging.info('Classification error: %s', err)
            return (False, 'Something went wrong when classifying the '
                           'image. Maybe try another one?')


def get_conf_matrix(model):

    labels = model.labels.tolist()
    print labels, len(labels)
    len_labels = len(labels)
    confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    unclassified_list = []
    true_classified_list = []
    false_classified_list = []

    test_file_path = MODEL_PATH_DICT['testFilePath']
    with open(test_file_path) as f:
        test_list = f.readlines()
    tracker = 0
    for test_line in test_list:
	tracker += 1
	logging.info(str(tracker)+'/'+str(len(test_list)))
        try:
	    filename = test_line.split(' ')[0]
            file_val = int(test_line.split(' ')[1])
#	    image = exifutil.open_oriented_im(filename)
            image = caffe.io.load_image(filename)
        except Exception as e:
	    print e.message
	    print "ERROR:" + filename
	    continue

        result = model.classify_image(image)
        if result[0]:  # Status
            top_result = result[1][0]
            label = top_result[0]
            conf = float(top_result[1])
	    if conf > 0.80:  # Our threshold
		if labels.index(label) == file_val:
		    true_str = str(filename) + ',' + label + ',' + str(conf) + '\n'
                    true_classified_list.append(true_str)
		else:
		    false_str = str(filename) + ',' + label + ',' + str(conf) + ',' + labels[file_val] + '\n'
                    false_classified_list.append(false_str)
		confident_confusion_mat[labels.index(label)][file_val] += 1
	    else:
		unclassified_str = str(filename) + ',' + label + ',' + str(conf) + ',' + labels[file_val] + '\n'
                unclassified_list.append(unclassified_str)
	    confusion_mat[labels.index(label)][file_val] += 1

    df = pd.DataFrame(confusion_mat, index=labels, columns=labels)
    df.to_csv('confusion_matrix.csv')
    df = pd.DataFrame(confident_confusion_mat, index=labels, columns=labels)
    df.to_csv('confident_confusion_matrix.csv')
#    df_map = pd.read_csv(MODEL_PATH_DICT['urlMapPath'], names=['id', 'ground_truth', 'tag', 'url', 'path'], header=None)

    with open('true_classified_list.csv', 'w+') as thefile:
	for item in true_classified_list:
	    thefile.write(item)
 #   df = pd.read_csv('true_classified_list.csv', names=['path', 'observed', 'confidence'], header=None)
 #   print df.shape
 #   print df_map.shape
 #   df = pd.merge(df, df_map, on=['path'])
 #   print df.shape
 #   df.to_csv('true_classified_list.csv', columns=['url', 'observed', 'confidence', 'path'],index=False)

    with open('false_classified_list.csv', 'w+') as thefile:
        for item in false_classified_list:
            thefile.write(item)
 #   df = pd.read_csv('false_classified_list.csv', names=['path', 'observed', 'confidence', 'ground_truth_'], header=None)
 #   print df.shape
 #   print df_map.shape
 #   df = pd.merge(df, df_map, on=['path'])
 #   print df.shape
 #   df.to_csv('false_classified_list.csv', columns=['url', 'ground_truth_', 'observed', 'confidence', 'path'],index=False)

    with open('unclassified_list.csv', 'w+') as thefile:
        for item in unclassified_list:
            thefile.write(item)
 #   df = pd.read_csv('unclassified_list.csv', names=['path', 'observed', 'confidence', 'ground_truth_'], header=None)
 #   print df.shape
 #   print df_map.shape
 #   df = pd.merge(df, df_map, on=['path'])
 #   print df.shape
 #   df.to_csv('unclassified_list.csv', columns=['url', 'ground_truth_', 'observed', 'confidence', 'path'],index=False)
    return True


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    parser = optparse.OptionParser()
    parser.add_option(
        '-g', '--gpu',
        help="use gpu mode",
        action='store_true', default=False)

    opts, args = parser.parse_args()

    model_details = MODEL_PATH_DICT
    # Initialize classifier + warm start by forward for allocation
    caffe_model = ModelClassifier(model_def_file=model_details['deployPath'],
                                  pretrained_model_file=model_details['modelPath'],
                                  mean_file=model_details['meanFilePath'], raw_scale=255.,
                                  class_labels_file=model_details['mapFilePath'], image_dim=256, gpu_mode=opts.gpu)
    caffe_model.net.forward()
    logging.info('Initialized the classfier')
    get_conf_matrix(caffe_model)
