#from __future__ import print_function, division

import os
os.environ["CUDA_VISIBLE_DEVICES"]="0" 
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
#import matplotlib.pyplot as plt
import time
import pandas as pd
from datetime import datetime
import json
import operator
from os.path import basename
import pickle 

from tqdm import tqdm
from PIL import Image
import time
import logging
import optparse
import numpy as np
import pandas as pd
from config import *
import glob
import os



# TO DO: Check if all file exists, else don't let the model through

class ModelClassifier(object):
    def __init__(self, pretrained_model_file, class_labels_file,path_to_store_output,gpu_mode):
        logging.info('Loading net and associated files...')
        self.path_to_store_output = path_to_store_output
        
        with open(class_labels_file) as f:
            lables_dict = json.load(f)
            sorted_labels = sorted(lables_dict.items(), key=operator.itemgetter(1))
            self.labels = [x[0] for x in sorted_labels]
            #self.labels = lables_dict
        logging.info('Read map file...  found labels are :' + str(self.labels))
        
        model_ft = models.resnet34(pretrained=False)
        num_ftrs = model_ft.fc.in_features
        n_classes = len(self.labels)
        model_ft.fc = nn.Linear(num_ftrs, n_classes)
        if gpu_mode:
            logging.info('Trying to acquire GPU')

            model_ft.cuda()
        
        logging.info('Loading pretrained model')
        
        checkpoint = torch.load(pretrained_model_file)
        epoch = checkpoint['epoch']
        best_acc = checkpoint['best_acc']
        model_ft.load_state_dict(checkpoint['state_dict'])
        logging.info('model ready for prediction')
        
        self.net = model_ft
        self.net.eval()
        self.data_transforms = {
            
            'val': transforms.Compose([
                transforms.Scale((224,224)),
                #transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        }
        self.transform = self.data_transforms['val']
        
        
            
            
    def image_loader(self,image_name):
        """load image, returns cuda tensor"""
        image = Image.open(image_name).convert('RGB')
        image = self.transform(image)
        image = Variable(image.cuda())
        image = image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
       
    
        return image #.cuda()  #assumes that you're using GPU
    def softmax(self,x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum()


    def classify_image(self, image_path):
        try:
            starttime = time.time()
            image = self.image_loader(image_path)
            print ('image_size:' + str(image.size() ) )
            outputs = self.net(image)
            
            _, preds = torch.max(outputs.data, 1)
            index = preds[0]
            endtime = time.time()

            predictions = self.labels[index]
            return True, index,self.softmax(outputs.data.cpu().numpy()) 

        except Exception as err:
            logging.info('Classification error: %s', err)
            return (False, 'Something went wrong when classifying the '
                           'image. Maybe try another one?')


def get_conf_matrix(model,path_to_store_output,confidenceThreshholdList,predictionStorePath,usePreviousPredictions):

    len_labels = len(model.labels)
    labels = model.labels
    print("labels : "+ str(labels))
    confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    
    all_results = []
    test_file_path = MODEL_PATH_DICT['testFilePath']
    with open(test_file_path) as f:
        test_list = f.readlines()
        
    if os.path.exists(predictionStorePath) and usePreviousPredictions:
        print("using previous predicted file...")
        with open(predictionStorePath, 'r') as f:
            #all_results = f.read().splitlines()
            all_results = pickle.load(f)
    else:
        tracker = 0
        print("classifying images")
        for test_line in tqdm(test_list):
            tracker += 1
            logging.info(str(tracker)+'/'+str(len(test_list)))
            
            filename = test_line.split(' ')[0]
            file_val = int(test_line.split(' ')[1])
            print("file value :"+str(file_val))

            result = model.classify_image(filename)
            all_results.append(result)
        
        if not os.path.exists(path_to_store_output):
            print("creating path to store evaluation files")
            os.makedirs(path_to_store_output)
        with open(predictionStorePath, 'wb') as f:
            pickle.dump(all_results, f)
        #with open(predictionStorePath, 'w') as predictionFile:
        #    predictionFile.write('\n'.join('{} {} {}'.format(x[0],x[1],x[2]) for x in all_results))
            #for result in all_results:
            #    predictionFile.write('\n'.join(result))
    print("calculating...")       
    for threshhold in tqdm(confidenceThreshholdList):
        confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
        confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    
        unclassified_list = []
        true_classified_list = []
        false_classified_list = []
        suffix = "th"+str(threshhold)
        print("using suffix : "+ suffix)
        path_to_store_output = os.path.join(path_to_store_output,suffix)
        print("current path to store : "+ str(path_to_store_output))
        
        if not os.path.exists(path_to_store_output):
            os.makedirs(path_to_store_output)
        file_index = 0   
        for result in all_results:
            print(result)
            filename = test_list[file_index].split(' ')[0]
            file_val = int(test_list[file_index].split(' ')[1])
            label = None
            if result[0]:  # Status
                top_result = result[1]
                label = result[1]
                print("label is :" +str(label))
                #print("result  is :"+str(result))
                conf = float(result[2][0][label])
                print("confidence from prediction : "+str(conf))
            #for conf in confidenceThreshholdList:
            if float(conf) > float(threshhold*0.01):  # Our threshold
                if label == file_val:
                    true_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) +',' +basename(filename).split(".")[0]+ '\n'
                    true_classified_list.append(true_str)
                else:
                    false_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) + ',' + str(labels[file_val]) +',' +basename(filename).split(".")[0]+ '\n'
                    false_classified_list.append(false_str)
                confident_confusion_mat[label][file_val] += 1
            else:
                unclassified_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) + ',' + str(labels[file_val])+',' +basename(filename).split(".")[0] + '\n'
                unclassified_list.append(unclassified_str)
            confusion_mat[label][file_val] += 1
            file_index = file_index+1
            #print("index = "+ str(file_index))
            #print(test_list[file_index])
                 
        logging.info('Writing confusion matrix and related files')
        
        df = pd.DataFrame(confusion_mat, index=model.labels, columns=model.labels)
        df.to_csv(os.path.join(path_to_store_output,'confusion_matrix_'+suffix+'.csv'))
        df = pd.DataFrame(confident_confusion_mat, index=model.labels, columns=model.labels)
        df.to_csv(os.path.join(path_to_store_output,'confident_confusion_matrix_'+suffix+'.csv'))
#    df_map = pd.read_csv(MODEL_PATH_DICT['urlMapPath'], names=['id', 'ground_truth', 'tag', 'url', 'path'], header=None)

        with open(os.path.join(path_to_store_output,'true_classified_list_'+suffix+'.csv'), 'w+') as thefile:
            for item in true_classified_list:
                thefile.write(item)

        with open(os.path.join(path_to_store_output,'false_classified_list_'+suffix+'.csv'), 'w+') as thefile:
            for item in false_classified_list:
                thefile.write(item)

        with open(os.path.join(path_to_store_output,'unclassified_list_'+suffix+'.csv'), 'w+') as thefile:
            for item in unclassified_list:
                thefile.write(item)
        path_to_store_output = path_to_store_output.replace(suffix, "")
        
    return True


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.ERROR)
    parser = optparse.OptionParser()
    parser.add_option(
        '-g', '--gpu',
        help="use gpu mode",
        action='store_true', default=False)

    opts, args = parser.parse_args()
    opts.gpu= True
    model_details = MODEL_PATH_DICT
    # Initialize classifier + warm start by forward for allocation
    if os.path.exists(model_details['predictionStorePath']) and model_details['usePreviousPredictions']:
        opts.gpu= False
    #getting lates model file from folder
    list_of_files = glob.glob(model_details['modelPath']) 
    print("using model path : "+ str(model_details['modelPath']))
    model_file = max(list_of_files, key=os.path.getctime)
    print("picking model file : "+str(model_file))
    pytorch_model = ModelClassifier(pretrained_model_file=model_file,
                                      class_labels_file=model_details['mapFilePath'], path_to_store_output = model_details['pathToStoreOutput'],gpu_mode=opts.gpu)
    #caffe_model.net.forward()
    logging.info('Initialized the classfier')
    get_conf_matrix(pytorch_model,model_details['pathToStoreOutput'],model_details['confidenceThreshholdList'],
                   model_details['predictionStorePath'],model_details['usePreviousPredictions'])
