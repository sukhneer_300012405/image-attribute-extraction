__author__ = '8372'

import os

evaluation_for = "test"
article_type= "dresses"
attribute = "print_pattern_type"  #'sleeve_style', 'sleeve_length', 'length', 'print_pattern_type', 'neck'
architecture_folder_name = "res34_0.01"
architecture_folder_name_base = "res34_0.0001_split6"
architecture_folder_name_new = "res34_3.5184372088832036e-05_split6/"    
ckpt_parent_folder = "ckpt_newdata_multimodel_varlr_split6_decrease_LR_0.8"
base_folder = '/rapid_data/myntra/myntra_data/'

REPO_DIRNAME = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../..')
MODEL_PATH_DICT = dict(description='Fine-tuned ImageNet model for back pose',
                       pathToStoreOutput='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/evaluation-for-labeling/'+evaluation_for,
                       mapFilePath='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/'+article_type+'_'+attribute+'_label_map.txt', 
modelPath='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/' + ckpt_parent_folder + '/' + architecture_folder_name+'/*',
#modelPath_base='/rapid_data/myntra/myntra_data/'+ article_type+'/'+article_type+'_'+attribute+'/' + ckpt_parent_folder + '/'  + 'base_network' + '/' +architecture_folder_name_base +'/epoch_1_batch32_ckpt.pth.tar',
modelPath_base= '/rapid_data/myntra/myntra_data/'+ article_type+'/'+article_type+'_'+'neck'+'/' + ckpt_parent_folder + '/'  + 'base_network' + '/' +architecture_folder_name_base +'/epoch_7_batch32_ckpt.pth.tar',
modelPath_new='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/' + ckpt_parent_folder + '/' + 'new_network' + '/'+ architecture_folder_name_new +'/epoch_10_batch32_ckpt.pth.tar',
#testFilePath='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/'+article_type+'_'+attribute+'_new_'+evaluation_for+'.txt',
#testFilePath='/rapid_data/trendsanalysis/data/unique_Instagram_dummy_labels.csv',
#trainFilePath='/rapid_data/img_text_embeddings/Tops/paths.csv',#### CHange this for input images.
testFilePath='/rapid_data/img_text_embeddings/Tops/paths_all_bbox.csv',#### CHange this for input images.
#predictionStorePath='/rapid_data/myntra/myntra_data/'+article_type+'/'+article_type+'_'+attribute+'/evaluation-for-labeling/'+evaluation_for+'/prediction.txt',
predictionStorePath='/rapid_data/img_text_embeddings/Tops/tops_resnet_before_pca.pkl',                     
usePreviousPredictions = True,
confidenceThreshholdList = [80],
                      multimodel = True,
                      resnet_split_layer = 6)

# attribute_list = ['length'] )
#,neck, 'sleeve_length', 'print_pattern_type', 'shape', 'sleeve_style', 'length'])
