#from __future__ import print_function, division

 
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
#import matplotlib.pyplot as plt
import time
import pandas as pd
from datetime import datetime
import json
import operator
from os.path import basename
import pickle 

from tqdm import tqdm
from PIL import Image
import time
import logging
import optparse
import numpy as np
import pandas as pd
from sahib_config import *
import glob
import os
import ImageFileList
#import sklearn
from sklearn.decomposition import PCA


# TO DO: Check if all file exists, else don't let the model through

class ModelClassifier(object):
    def __init__(self, pretrained_model_file, class_labels_file,path_to_store_output,gpu_mode,multimodel,resnet_split_layer):
        logging.info('Loading net and associated files...')
        self.path_to_store_output = path_to_store_output
        
        with open(class_labels_file) as f:
            lables_dict = json.load(f)
            sorted_labels = sorted(lables_dict.items(), key=operator.itemgetter(1))
            self.labels = [x[0] for x in sorted_labels]
            #self.labels = lables_dict
        logging.info('Read map file...  found labels are :' + str(self.labels))
        
        model_ft = models.resnet34(pretrained=False)
        num_ftrs = model_ft.fc.in_features
        n_classes = len(self.labels)
        model_ft.fc = nn.Linear(num_ftrs, n_classes)
        if gpu_mode:
            logging.info('Trying to acquire GPU')

            model_ft.cuda()
        
        logging.info('Loading pretrained model')
        if multimodel:
            model_ft=self.load_multimodel_ckpts(model_ft,resnet_split_layer,pretrained_model_file[0], pretrained_model_file[1])
        else:
            checkpoint = torch.load(pretrained_model_file)
            epoch = checkpoint['epoch']
            best_acc = checkpoint['best_acc']
            model_ft.load_state_dict(checkpoint['state_dict'])
        logging.info('model ready for prediction')
        
        self.net = model_ft
        '''
        Information about model's layers:
        
        for i,ch in enumerate(list(model_ft.children())):
            print i,ch
        import sys;sys.exit()
        '''
        
        self.net.eval()
        self.data_transforms = {
            
            'val': transforms.Compose([
                transforms.Scale((224,224)),
                #transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        }
        self.transform = self.data_transforms['val']
        self.gpu_mode = gpu_mode
        
            
            
    def image_loader(self,image_name):
        """load image, returns cuda tensor"""
        image = Image.open(image_name).convert('RGB')
        image = self.transform(image)
        image = Variable(image.cuda())
        image = image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
       
    
        return image #.cuda()  #assumes that you're using GPU
    def softmax(self,x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum()

    def add_max_pool(self,model ):
        model =self.MaxPool2d(kernel_size=7,stride=1)
        
    def classify_image(self, data,count):
        try:
            inputs, labels = data
            if self.gpu_mode:
                    inputs = Variable(inputs.cuda())
                    labels = Variable(labels.cuda())
            else:
                    inputs, labels = Variable(inputs), Variable(labels)

            #optimizer.zero_grad() 

            class MaxPool(nn.Module):
                def __init__(self):
                    super(MaxPool,self).__init__()
                    self.maxpool=nn.MaxPool2d(kernel_size=4,stride=4, padding = 2)
                def forward(self,x):
                    x=self.maxpool(x)
                    return x

            count+=1
            if count==1:
                new_classifier=nn.Sequential(*list(self.net.children())[:-3])########

                self.net = new_classifier
                self.net.max_pool = MaxPool()

                    #self.net.nn.MaxPool2d(kernel_size=7,stride=1)
                    #print self.net,'network'

            outputs = self.net(inputs)
            import numpy
            #print outputs
            out = outputs.cpu().data.numpy()
            print out.shape
            return out.flatten() ,count
            '''
            _, preds = torch.max(outputs.data, 1)
            softmax = nn.Softmax()

            #return zip(preds.cpu().numpy(),self.softmax(outputs.data.cpu().numpy()))
            return zip(preds.cpu().numpy(),softmax(outputs).data.cpu().numpy())
            '''
        except Exception as err:
            print("exception occured as : "+str(err))
            logging.info('Classification error: %s', err)
            return (False, 'Something went wrong when classifying the '
                           'image. Maybe try another one?')
        
    def load_multimodel_ckpts(self,model,resnet_split_layer,file_path_base_ckpt, file_path_new_network_ckpt):
            model_full_state_dict = model.state_dict()
            if file_path_base_ckpt != None and os.path.exists(file_path_base_ckpt):
                print("Loading base checkpoint, from :"+str(file_path_base_ckpt))
                base_model_ckpt = torch.load(file_path_base_ckpt)
                base_model_state_dict = base_model_ckpt['state_dict']
                model_full_state_dict.update(base_model_state_dict)
                
            if file_path_new_network_ckpt != None and os.path.exists(file_path_new_network_ckpt):
                print("Loading new checkpoint, from :"+str(file_path_new_network_ckpt))
                new_network_model_ckpt = torch.load(file_path_new_network_ckpt)
                new_network_model_state_dict = new_network_model_ckpt['state_dict']
                model_full_state_dict.update(new_network_model_state_dict)
                

            model.load_state_dict(model_full_state_dict)        
            #optimizer.load_state_dict(optimizer_full_state_dict)

            return model

def get_conf_matrix(model,path_to_store_output,confidenceThreshholdList,predictionStorePath,usePreviousPredictions,batch_size_num =32):
    train_val_list = [ 'val']
    # data_dir = 'hymenoptera_data'
    path_to_input_file = {
        
        'val': MODEL_PATH_DICT['testFilePath']
        }
    

    image_datasets = {x: ImageFileList.ImageFilelisit_a( flist=path_to_input_file[x],
                                       transform = model.data_transforms[x])
                      for x in train_val_list}
    '''
    image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x),
                                              data_transforms[x])
                      for x in ['train', 'val']}
    '''

    dataloders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=1,
                                                 shuffle=False, num_workers=4)
                  for x in train_val_list}


    len_labels = len(model.labels)
    labels = model.labels
    confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    
    all_results = []
    test_file_path = MODEL_PATH_DICT['testFilePath']
    with open(test_file_path) as f:
        test_list = f.readlines()
        
    if False:
        print("using previous predicted file...")
        with open(predictionStorePath, 'r') as f:
            #all_results = f.read().splitlines()
            all_results = pickle.load(f)
    else:
        #tracker = 0
        print("classifying images in batch")
        print("size of validation file : " + str(len(dataloders["val"])))
        print("size of all results : "+ str(len(all_results)))
        count =0
        for data in tqdm(dataloders["val"]):
            result,count = model.classify_image(data,count)
            all_results.append(result)
        print len(all_results)
        print all_results[0].shape
        #print 'starting PCA'
        #from sklearn.decomposition import PCA
        #pca  = PCA(n_components =5000 )
        #fvT = pca.fit_transform(all_results)
        #print fvT.shape
        
        
        
        print 'aa'
        if not os.path.exists(path_to_store_output):
            print("creating path to store evaluation files")
            os.makedirs(path_to_store_output)
        with open(predictionStorePath, 'wb') as f:
            pickle.dump(all_results, f)
        print 'done'
        import sys;sys.exit()
        #with open(predictionStorePath, 'w') as predictionFile:
        #    predictionFile.write('\n'.join('{} {} {}'.format(x[0],x[1],x[2]) for x in all_results))
            #for result in all_results:
            #    predictionFile.write('\n'.join(result))
    '''
    
    print("calculating...")       
    for threshhold in tqdm(confidenceThreshholdList):
        confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
        confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    
        unclassified_list = []
        true_classified_list = []
        false_classified_list = []
        suffix = "th"+str(threshhold)
        print("using suffix : "+ suffix)
        path_to_store_output = os.path.join(path_to_store_output,suffix)
        print("current path to store : "+ str(path_to_store_output))
        
        if not os.path.exists(path_to_store_output):
            os.makedirs(path_to_store_output)
        file_index = 0   
        #print("value of all results : " + str(all_results))
        for result in all_results:
            #print("value of result is :"+ str(result))
            filename = test_list[file_index].split(' ')[0]
            file_val = int(test_list[file_index].split(' ')[1])
            label = None
            
            top_result = result[0]
            label = result[0]
            #print("label is :" +str(label))
            
            #print("result  is :"+str(result))
            conf = float(result[1][label])
            
            #for conf in confidenceThreshholdList:
            if float(conf) > float(threshhold*0.01):  # Our threshold
                if label == file_val:
                    true_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) +',' +basename(filename).split(".")[0]+ '\n'
                    true_classified_list.append(true_str)
                else:
                    false_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) + ',' + str(labels[file_val]) +',' +basename(filename).split(".")[0]+ '\n'
                    false_classified_list.append(false_str)
                confident_confusion_mat[label][file_val] += 1
            else:
                unclassified_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) + ',' + str(labels[file_val])+',' +basename(filename).split(".")[0] + '\n'
                unclassified_list.append(unclassified_str)
            confusion_mat[label][file_val] += 1
            file_index = file_index+1
            #print("index = "+ str(file_index))
            #print(test_list[file_index])
       
        logging.info('Writing confusion matrix and related files')
        
        df = pd.DataFrame(confusion_mat, index=model.labels, columns=model.labels)
        df.to_csv(os.path.join(path_to_store_output,'confusion_matrix_'+suffix+'.csv'))
        df = pd.DataFrame(confident_confusion_mat, index=model.labels, columns=model.labels)
        df.to_csv(os.path.join(path_to_store_output,'confident_confusion_matrix_'+suffix+'.csv'))
#    df_map = pd.read_csv(MODEL_PATH_DICT['urlMapPath'], names=['id', 'ground_truth', 'tag', 'url', 'path'], header=None)

        with open(os.path.join(path_to_store_output,'true_classified_list_'+suffix+'.csv'), 'w+') as thefile:
            for item in true_classified_list:
                thefile.write(item)

        with open(os.path.join(path_to_store_output,'false_classified_list_'+suffix+'.csv'), 'w+') as thefile:
            for item in false_classified_list:
                thefile.write(item)

        with open(os.path.join(path_to_store_output,'unclassified_list_'+suffix+'.csv'), 'w+') as thefile:
            for item in unclassified_list:
                thefile.write(item)
        path_to_store_output = path_to_store_output.replace(suffix, "")
        
        correct_predictions=0
        for index in range(0,confusion_mat.shape[0]):
            correct_predictions= confusion_mat[index][index]+correct_predictions
        accuracy = float(correct_predictions)/float(len(all_results))
        print("Accuracy is : "+ str(accuracy))
    '''
    print 'done'
    return True


if __name__ == '__main__':
    #multimodel = False
    
    logging.getLogger().setLevel(logging.ERROR)
    parser = optparse.OptionParser()
    model_details = MODEL_PATH_DICT
    '''
    parser.add_option(
        '-g', '--gpu',
        help="use gpu mode",
        action='store_true', default=False)

    
    
    opts, args = parser.parse_args()
    
    opts.gpu= True
    
    model_details = MODEL_PATH_DICT
    # Initialize classifier + warm start by forward for allocation
    if os.path.exists(model_details['predictionStorePath']) and model_details['usePreviousPredictions']:
        opts.gpu= False
    '''
    #getting lates model file from folder
    multimodel = model_details['multimodel']
    
    if multimodel:
        print("model base path : " +str(model_details['modelPath_base']))
        model_file = []
        list_of_files = glob.glob(model_details['modelPath_base']) 
        model_file.append(max(list_of_files, key=os.path.getctime))
        
        print("model new path : " +str(model_details['modelPath_new']))
        
        list_of_files = glob.glob(model_details['modelPath_new']) 
        model_file.append(max(list_of_files, key=os.path.getctime))
    else:    
        list_of_files = glob.glob(model_details['modelPath']) 
        model_file = max(list_of_files, key=os.path.getctime)
        print("picking model file : "+str(model_file))
    pytorch_model = ModelClassifier(pretrained_model_file=model_file,
                                      class_labels_file=model_details['mapFilePath'], path_to_store_output = model_details['pathToStoreOutput'],gpu_mode=True, multimodel=multimodel,resnet_split_layer = model_details['resnet_split_layer'])
    #caffe_model.net.forward()
    logging.info('Initialized the classfier')
    get_conf_matrix(pytorch_model,model_details['pathToStoreOutput'],model_details['confidenceThreshholdList'],
                   model_details['predictionStorePath'],model_details['usePreviousPredictions'])

