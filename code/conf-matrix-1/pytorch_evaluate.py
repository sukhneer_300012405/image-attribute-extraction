__author__ = '8372'

import time
import logging
import optparse
import numpy as np
import pandas as pd
from config_back import *
import exifutil
import caffe


# TO DO: Check if all file exists, else don't let the model through

class ModelClassifier(object):
    def __init__(self, model_def_file, pretrained_model_file, mean_file,
                 raw_scale, class_labels_file, image_dim, gpu_mode):
        logging.info('Loading net and associated files...')
        if gpu_mode:
            caffe.set_mode_gpu()
            caffe.set_device(1)
        else:
            caffe.set_mode_cpu()
        self.net = caffe.Classifier(
            model_def_file, pretrained_model_file,
            image_dims=(image_dim, image_dim), raw_scale=raw_scale,
            mean=np.load(mean_file).mean(1).mean(1), channel_swap=(2, 1, 0)
        )

        with open(class_labels_file) as f:
            labels_df = pd.DataFrame([
                                         {
                                             'id': l.strip().split(' ')[0][1:],
                                             # The [1:] to get rid of 'n' in labels of the format n<label_id>
                                             'name': ' '.join(l.strip().split(' ')[1:]).split(',')[0]
                                         }
                                         for l in f.readlines()
                                         ])
            labels_df = labels_df.convert_objects(convert_numeric=True)
        self.labels = labels_df.sort('id')['name'].values

    def classify_image(self, image):
        try:
            starttime = time.time()
            scores = self.net.predict([image], oversample=True).flatten()
            endtime = time.time()

            indices = (-scores).argsort()[:1]
            predictions = self.labels[indices]

            # In addition to the prediction text, we will also produce
            # the length for the progress bar visualization.
            meta = [
                (p, '%.5f' % scores[i])
                for i, p in zip(indices, predictions)
                ]
            logging.info('result: %s', str(meta))

            return True, meta, '%.3f' % (endtime - starttime)

        except Exception as err:
            logging.info('Classification error: %s', err)
            return (False, 'Something went wrong when classifying the '
                           'image. Maybe try another one?')


def get_conf_matrix(model):

    labels = model.labels.tolist()
    len_labels = len(labels)
    confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
    unclassified_list = []
    back_classified_list = []
    frontal_classified_list = []

    test_file_path = MODEL_PATH_DICT['fileMapPath']
    map_df = pd.read_csv(test_file_path, header=None, names=['style_id', 'tag', 'url', 'path'])
    full_list = map_df['path'].tolist()
    test_list = full_list
    map_df['pose']=''
    map_df['pose_conf'] = 0.0
    print len(test_list)
    tracker = 0
    for filename in test_list:
	tracker += 1
	logging.info(str(tracker)+'/'+str(len(test_list)))
        try :
            image = caffe.io.load_image(filename)
	except Exception as e:
            print e.message
            print "ERROR:" + filename
            continue

        result = model.classify_image(image)
        if result[0]:  # Status
            top_result = result[1][0]
            label = top_result[0]
            conf = float(top_result[1])
	    map_df.set_value(full_list.index(filename), 'pose', label)
	    map_df.set_value(full_list.index(filename), 'pose_conf', conf)
    map_df = map_df[~map_df['pose_conf'].isin([0.0])]
    map_df.to_csv('kids_dresses_full_filemap_pose.csv', columns=['style_id', 'tag', 'url', 'path', 'pose', 'pose_conf'],index=False)
    return True


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    parser = optparse.OptionParser()
    parser.add_option(
        '-g', '--gpu',
        help="use gpu mode",
        action='store_true', default=False)

    opts, args = parser.parse_args()

    model_details = MODEL_PATH_DICT
    # Initialize classifier + warm start by forward for allocation
    caffe_model = ModelClassifier(model_def_file=model_details['deployPath'],
                                  pretrained_model_file=model_details['modelPath'],
                                  mean_file=model_details['meanFilePath'], raw_scale=255.,
                                  class_labels_file=model_details['mapFilePath'], image_dim=256, gpu_mode=opts.gpu)

    caffe_model.net.forward()
    logging.info('Initialized the classfier')
    get_conf_matrix(caffe_model)
