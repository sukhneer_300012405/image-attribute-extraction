#from __future__ import print_function, division

import os
os.environ["CUDA_VISIBLE_DEVICES"]="0" 
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import time
import pandas as pd
from datetime import datetime
import json
import operator
from os.path import basename
import pickle 
from tqdm import tqdm
from PIL import Image
import time
import logging
import optparse
import numpy as np
import pandas as pd
from multi_label_multi_output_config import *
import glob
import os
import ImageFileList
from Resnet_branched_generic import *

# TO DO: Check if all file exists, else don't let the model through

class ModelClassifier(object):
    def __init__(self, pretrained_model_file, class_labels_file, path_to_store_output, gpu_mode, n_classes_dict):
               
        logging.info('Loading net and associated files...')
#         self.path_to_store_output = path_to_store_output
        self.path_to_store_output = {}
        self.labels = {}
        for attr in attributes:
            with open(class_labels_file[attr]) as f:
                lables_dict = json.load(f)
                sorted_labels = sorted(lables_dict.items(), key=operator.itemgetter(1))
                self.labels[attr] = [x[0] for x in sorted_labels]
                self.path_to_store_output[attr] = path_to_store_output[attr]
            #self.labels = lables_dict
        logging.info('Read map file...  found labels are :' + str(self.labels))

        model_ft = Resnet_branched(n_classes_dict, attributes)
        
        if gpu_mode:
            logging.info('Trying to acquire GPU')
            model_ft.cuda()
        
        logging.info('Loading pretrained model')
        checkpoint = torch.load(pretrained_model_file)
        epoch = checkpoint['epoch']
        best_acc = checkpoint['best_acc']
        model_ft.load_state_dict(checkpoint['state_dict'])
        logging.info('model ready for prediction')
        
        self.net = model_ft
        self.net.eval()
        self.data_transforms = {
            
            'val': transforms.Compose([
                transforms.Scale((224,224)),
                #transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        }
        self.transform = self.data_transforms['val']
        self.gpu_mode = gpu_mode       
            
    def image_loader(self,image_name):
        """load image, returns cuda tensor"""
        image = Image.open(image_name).convert('RGB')
        image = self.transform(image)
        image = Variable(image.cuda())
        image = image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
        return image #.cuda()  #assumes that you're using GPU
    
    def softmax(self,x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum()

    def classify_image(self, data):
        inputs, labels = data
        labels_list = []
        for l in labels:
            l = list(l)
            l = torch.LongTensor([int(x) for x in l])
            l = Variable(l).cuda()
            labels_list.append(l)   

        attributes_dict ={}
        for i, att in enumerate(attributes):
            attributes_dict[att] = labels_list[i]

        inputs = Variable(inputs.cuda())

        # forward
        outputs = self.net(inputs)
        softmax = nn.Softmax()
        results_dict = {}
        for i, attr in enumerate(attributes):
            prob = softmax(outputs[i]).data
            _ , pred = torch.max(outputs[i].data, 1)
            labels = attributes_dict[attr].data
            results_dict[attr] = zip(pred, prob, labels) 
        return results_dict

def get_conf_matrix(model,path_to_store_output,confidenceThreshholdList,predictionStorePath,usePreviousPredictions,batch_size_num =32):
    train_val_list = [ 'val']
    path_to_input_file = {
        
        'val': MODEL_PATH_DICT['testFilePath']
        }

    image_datasets = {x: ImageFileList.ImageFilelisit_a( flist=path_to_input_file[x],
                                       transform = model.data_transforms[x])
                      for x in train_val_list}

    dataloders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=batch_size_num,
                                                 shuffle=False, num_workers=4)
                  for x in train_val_list}

    for attr in attributes:
        print ('Running inference for {}'.format(attr))
        path_output = path_to_store_output[attr]
        len_labels = len(model.labels[attr])
        labels = model.labels[attr]
        confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
        confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)

        all_results = []
        test_file_path = MODEL_PATH_DICT['testFilePath']
        with open(test_file_path) as f:
            test_list = f.readlines()

#         if os.path.exists(predictionStorePath[attr]) and usePreviousPredictions:
        if False:
            print("using previous predicted file...")
            with open(predictionStorePath[attr], 'r') as f:
                #all_results = f.read().splitlines()
                all_results = pickle.load(f)
        else:
            print("classifying images in batch")
            count = 0
            for data in tqdm(dataloders["val"]):
                count += 1
                result = model.classify_image(data)
                result = result[attr]
                all_results.extend(result)

            print("size of validation file : " + str(len(dataloders["val"])))
            print("size of all results : "+ str(len(all_results)))

            if not os.path.exists(path_output):
                print("creating path to store evaluation files")
                os.makedirs(path_output)
            with open(predictionStorePath[attr], 'wb') as f:
                pickle.dump(all_results, f)
                
        print("calculating...")       
        for threshhold in tqdm(confidenceThreshholdList):
            confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
            confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
            unclassified_list = []
            true_classified_list = []
            false_classified_list = []
            suffix = "th"+str(threshhold)
            print("using suffix : "+ suffix)
            path_output = os.path.join(path_output,suffix)
            print("current path to store : "+ str(path_output))

            if not os.path.exists(path_output):
                os.makedirs(path_output)
            file_index = 0   
            #print("value of all results : " + str(all_results))
            for result in all_results:
                filename = test_list[file_index].split(' ')[0]
#                 file_val = int(test_list[file_index].split(' ')[1])
                file_val = result[2]
                label = result[0]  # prediction
                conf , _ = torch.max(result[1], 0)
            
                #for conf in confidenceThreshholdList:
                if float(conf) > float(threshhold*0.01):  # Our threshold
                    if label == file_val:
                        true_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) +',' +basename(filename).split(".")[0]+ '\n'
                        true_classified_list.append(true_str)
                    else:
                        false_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) + ',' + str(labels[file_val]) +',' +basename(filename).split(".")[0]+ '\n'
                        false_classified_list.append(false_str)
                    confident_confusion_mat[label][file_val] += 1
                else:
                    unclassified_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) + ',' + str(labels[file_val])+',' +basename(filename).split(".")[0] + '\n'
                    unclassified_list.append(unclassified_str)
                confusion_mat[label][file_val] += 1
                file_index = file_index+1

            logging.info('Writing confusion matrix and related files')
            df = pd.DataFrame(confusion_mat, index=model.labels[attr], columns=model.labels[attr])
            df.to_csv(os.path.join(path_output,'confusion_matrix_'+suffix+'.csv'))
            df = pd.DataFrame(confident_confusion_mat, index=model.labels[attr], columns=model.labels[attr])
            df.to_csv(os.path.join(path_output,'confident_confusion_matrix_'+suffix+'.csv'))

            with open(os.path.join(path_output,'true_classified_list_'+suffix+'.csv'), 'w+') as thefile:
                for item in true_classified_list:
                    thefile.write(item)

            with open(os.path.join(path_output,'false_classified_list_'+suffix+'.csv'), 'w+') as thefile:
                for item in false_classified_list:
                    thefile.write(item)

            with open(os.path.join(path_output,'unclassified_list_'+suffix+'.csv'), 'w+') as thefile:
                for item in unclassified_list:
                    thefile.write(item)
            path_output = path_output.replace(suffix, "")

            correct_predictions=0
            for index in range(0,confusion_mat.shape[0]):
                correct_predictions = confusion_mat[index][index] + correct_predictions
            accuracy = float(correct_predictions)/float(len(all_results))
            print("Accuracy is : "+ str(accuracy))
            
    return True

if __name__ == '__main__':
    #multimodel = False
    logging.getLogger().setLevel(logging.ERROR)
    parser = optparse.OptionParser()
    parser.add_option(
        '-g', '--gpu',
        help="use gpu mode",
        action='store_true', default=False)

    opts, args = parser.parse_args()
    opts.gpu= True
    model_details = MODEL_PATH_DICT
    
    ## create dictionary for attributes and corresponding classes count
    print ('MODEL_PATH_DICT', MODEL_PATH_DICT['trainFilePath'])
    df = pd.read_csv(MODEL_PATH_DICT['trainFilePath'], header = None, sep =' ')
    df.columns = ['path'] + attributes
    n_classes_dict = {}
    for attrib in attributes:
        n_classes_dict[attrib] = df[attrib].nunique()
            
    list_of_files = glob.glob(model_details['modelPath']) 
    model_file = max(list_of_files, key=os.path.getctime)
    print("picking model file : "+str(model_file))
    pytorch_model = ModelClassifier(pretrained_model_file=model_file,
                                      class_labels_file=model_details['mapFilePath'], path_to_store_output = model_details['pathToStoreOutput'],gpu_mode=opts.gpu, n_classes_dict = n_classes_dict)

    logging.info('Initialized the classfier')
    get_conf_matrix(pytorch_model,model_details['pathToStoreOutput'],model_details['confidenceThreshholdList'],
                   model_details['predictionStorePath'],model_details['usePreviousPredictions'])
