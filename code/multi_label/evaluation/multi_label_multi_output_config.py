import os
import pandas as pd

evaluation_for = "test"
article_type= "jeans"
# attributes = ['sleeve_style', 'sleeve_length', 'print_pattern_type', 'neck']
# attributes = ['sleeve_style','sleeve_length','length','print_pattern_type','neck','hemline', 'shape'] #for dresses
attributes =  ['fade','distress','shade']
architecture_folder_name = "res34_0.01"   
ckpt_parent_folder = '/multi_label/ckpt_unaug_data_convbranch_generic/'
base_folder = '/rapid_data/myntra/myntra_data/'

REPO_DIRNAME = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../..')

mapFilePath_dict = {}
predictionStorePath_dict = {}
pathToStoreOutput_dict = {}

for attr in attributes:
    mapFilePath_dict[attr] = base_folder+article_type +'/' + article_type +'_'+attr+'/'+ article_type +'_'+ attr + '_label_map.txt'
    predictionStorePath_dict[attr] = base_folder + article_type + '/' + article_type + '_' + attr +'/evaluation-multioutput_multiclass/'+ evaluation_for + '/predictions.txt'
    pathToStoreOutput_dict[attr] = base_folder + article_type + '/' + article_type + '_' + attr + '/evaluation-multioutput_multiclass/'+ evaluation_for

MODEL_PATH_DICT = dict(   
description='Branched Resnet',
pathToStoreOutput = pathToStoreOutput_dict,
mapFilePath = mapFilePath_dict,
modelPath = base_folder + article_type + ckpt_parent_folder + architecture_folder_name + '/*',
testFilePath = base_folder + article_type + '/multi_label/multi_label_' + evaluation_for + '.txt',
trainFilePath = base_folder + article_type + '/multi_label/multi_label_train.txt',
predictionStorePath = predictionStorePath_dict,
usePreviousPredictions = False,
confidenceThreshholdList = [80,70,60,50], multimodel = False, resnet_split_layer = 6
                        )

# attribute_list = ['length'] )
#,neck, 'sleeve_length', 'print_pattern_type', 'shape', 'sleeve_style', 'length'])
