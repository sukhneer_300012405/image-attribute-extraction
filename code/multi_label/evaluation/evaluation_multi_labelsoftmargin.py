#from __future__ import print_function, division

import os
os.environ["CUDA_VISIBLE_DEVICES"]="0" 
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
#import matplotlib.pyplot as plt
import time
import pandas as pd
from datetime import datetime
import json
import operator
from os.path import basename
import pickle 
from torch import sigmoid

from tqdm import tqdm
from PIL import Image
import time
import logging
import optparse
import numpy as np
import pandas as pd
from multi_label_config import *
import glob
import os
import ImageFileList
from torchvision.models.resnet import BasicBlock

# TO DO: Check if all file exists, else don't let the model through

class ModelClassifier(object):
    def __init__(self, pretrained_model_file, class_labels_file, path_to_store_output, gpu_mode):
               
        logging.info('Loading net and associated files...')
#         self.path_to_store_output = path_to_store_output
        self.path_to_store_output = {}
        self.labels = {}
        for attr in attributes:
            with open(class_labels_file[attr]) as f:
                lables_dict = json.load(f)
                sorted_labels = sorted(lables_dict.items(), key=operator.itemgetter(1))
                self.labels[attr] = [x[0] for x in sorted_labels]
                self.path_to_store_output[attr] = path_to_store_output[attr]
            #self.labels = lables_dict
        logging.info('Read map file...  found labels are :' + str(self.labels))
        
#         model_ft = models.resnet34(pretrained=False)
#         num_ftrs = model_ft.fc.in_features
#         n_classes = len(self.labels)
#         model_ft.fc = nn.Linear(num_ftrs, n_classes)
        
        model_ft = models.resnet34(pretrained=True)
        num_ftrs = model_ft.fc.in_features
        del model_ft

        NUM_CLASSES = len(overall_df.columns) - 1
        print ('NUM_CLASSES:' + str(NUM_CLASSES))
        FIRST_FC_NODES = 1024

        class MyResnet2(models.ResNet):
            def __init__(self, block, layers, num_classes = 1000):
                super(MyResnet2, self).__init__(block, layers, num_classes)

                model_ft = models.resnet34(pretrained=True)
                model_state_dict = model_ft.state_dict()
                self.load_state_dict(model_state_dict)

                self.fc = nn.Linear(num_ftrs, FIRST_FC_NODES)
                self.fc1 = nn.Linear(FIRST_FC_NODES , NUM_CLASSES)


            def forward(self, x):
                x = self.conv1(x)
                x = self.bn1(x)
                x = self.relu(x)
                x = self.maxpool(x)

                x = self.layer1(x)
                x = self.layer2(x)
                x = self.layer3(x) 
                x = self.layer4(x)
                x = self.avgpool(x)
                x = x.view(x.size(0), -1)
                x = self.fc(x)
                x = self.fc1(x)

                return x

        model_ft = MyResnet2(BasicBlock, [3, 4, 6, 3], 1000)
        
        if gpu_mode:
            logging.info('Trying to acquire GPU')
            model_ft.cuda()
        
        logging.info('Loading pretrained model')
        checkpoint = torch.load(pretrained_model_file)
        epoch = checkpoint['epoch']
        best_acc = checkpoint['best_acc']
        model_ft.load_state_dict(checkpoint['state_dict'])
        logging.info('model ready for prediction')
        
        self.net = model_ft
        self.net.eval()
        self.data_transforms = {
            
            'val': transforms.Compose([
                transforms.Scale((224,224)),
                #transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        }
        self.transform = self.data_transforms['val']
        self.gpu_mode = gpu_mode
        
            
            
    def image_loader(self,image_name):
        """load image, returns cuda tensor"""
        image = Image.open(image_name).convert('RGB')
        image = self.transform(image)
        image = Variable(image.cuda())
        image = image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet

        return image #.cuda()  #assumes that you're using GPU
    
    def softmax(self,x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum()


    def classify_image(self, data):

        try:
            inputs, labels = data
            labels_list = []
            for l in labels:
                l = list(l)
                l = torch.FloatTensor([int(x) for x in l])
                l = Variable(l).cuda()
                labels_list.append(l)   
            
            OHE_attributes_dict ={}
            for i, att in enumerate(OHE_attributes):
                OHE_attributes_dict[att] = labels_list[i]
                    
#             if self.gpu_mode:
#                     inputs = Variable(inputs.cuda())
#                     labels = Variable(labels.cuda())
#             else:
#                     inputs, labels = Variable(inputs), Variable(labels)
                    
            inputs = Variable(inputs.cuda())

            #optimizer.zero_grad() 
            
            outputs = self.net(inputs)
            outputs = outputs.transpose(-2,1) #transpose output to 79 * 32 dim
            preds = {}
            for i, attrib in enumerate(OHE_attributes):
                prob = sigmoid(outputs[i])  
                preds[attrib] = np.array(prob.data) # 79 * 32
               
            results_dict = {}
            for i, attr in enumerate(attributes):
                # OHE attribute names, list of len 5
                att_classes = [x for x in OHE_attributes if x.startswith(attr)] 
                
                # OHE attribute values, list of 5 sublists each having 32 elements
                att_classes_list = [OHE_attributes_dict[x].data.cpu().numpy() for x in att_classes ] 
                
                # prediction for OHE attributes, list of 5 sublists each having 32 elements
                att_classes_prob = [preds[x] for x in att_classes] 
                
                # reshaping list to have 32 sublists each having 5 elements
                att_classes_prob = [list(x) for x in zip(*att_classes_prob)]  
                
                attribute_predictions = np.argmax(att_classes_prob, axis = 1)
#                 attribute_prob = np.max(att_classes_pred, axis = 0)
                actual_labels = np.argmax(att_classes_list, axis = 0)
                results_dict[attr] = zip(attribute_predictions,att_classes_prob,actual_labels) 
#                 print ('att:', att)
#                 print ('att_classes_list:', len(att_classes_list), len(att_classes_list[0]) )
#                 print ('att_classes_prob:', len(att_classes_prob) )
#                 print ('attribute_predictions:', attribute_predictions.shape)
#                 print ('actual_labels:', actual_labels.shape )
#                 running_corrects[att] += np.sum(attribute_predictions == actual_labels)
#                 print (att, np.mean(attribute_predictions[att] == actual_labels[att] ) )

#             outputs = self.net(inputs)
#             _, preds = torch.max(outputs.data, 1)
#             softmax = nn.Softmax()

            #return zip(preds.cpu().numpy(),self.softmax(outputs.data.cpu().numpy()))
            return results_dict

        except Exception as err:
            print("exception occured as : "+str(err))
            logging.info('Classification error: %s', err)
            return (False, 'Something went wrong when classifying the '
                           'image. Maybe try another one?')

def get_conf_matrix(model,path_to_store_output,confidenceThreshholdList,predictionStorePath,usePreviousPredictions,batch_size_num =32):
    train_val_list = [ 'val']
    # data_dir = 'hymenoptera_data'
    path_to_input_file = {
        
        'val': MODEL_PATH_DICT['testFilePath']
        }

    image_datasets = {x: ImageFileList.ImageFilelisit_a( flist=path_to_input_file[x],
                                       transform = model.data_transforms[x])
                      for x in train_val_list}
    '''
    image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x),
                                              data_transforms[x])
                      for x in ['train', 'val']}
    '''

    dataloders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=batch_size_num,
                                                 shuffle=False, num_workers=4)
                  for x in train_val_list}

    for attr in attributes:
        print ('Running inference for {}'.format(attr))
        path_output = path_to_store_output[attr]
        len_labels = len(model.labels[attr])
        labels = model.labels[attr]
        confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
        confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)

        all_results = []
        test_file_path = MODEL_PATH_DICT['testFilePath']
        with open(test_file_path) as f:
            test_list = f.readlines()

#         if os.path.exists(predictionStorePath[attr]) and usePreviousPredictions:
        if False:
            print("using previous predicted file...")
            with open(predictionStorePath[attr], 'r') as f:
                #all_results = f.read().splitlines()
                all_results = pickle.load(f)
        else:
            #tracker = 0
            print("classifying images in batch")

            count = 0
            for data in tqdm(dataloders["val"]):
                count += 1
                result = model.classify_image(data)
#                 print ('result', type(result))
                result = result[attr]
                all_results.extend(result)
#                 print(result.keys())
#                 results = results[attr]
#                 print ('len(data)',len(data) , len(pred[attr]), len(prob[attr][0]), len(actual[attr]) )
#                 print (attr, np.mean([1 if x == y else 0 for x,y in zip(actual[attr],pred[attr])]))
#                 result = zip(pred[attr], prob[attr], actual[attr])
#                 print ('len of result[attr]' , len(result) , result)
                
                #if tracker>3:
                #    break
                #tracker = tracker+1
                
#             print ('all_results', len(all_results), all_results[0])
            print("size of validation file : " + str(len(dataloders["val"])))
            print("size of all results : "+ str(len(all_results)))

            if not os.path.exists(path_output):
                print("creating path to store evaluation files")
                os.makedirs(path_output)
            with open(predictionStorePath[attr], 'wb') as f:
                pickle.dump(all_results, f)
            #with open(predictionStorePath, 'w') as predictionFile:
            #    predictionFile.write('\n'.join('{} {} {}'.format(x[0],x[1],x[2]) for x in all_results))
                #for result in all_results:
                #    predictionFile.write('\n'.join(result))
        print("calculating...")       
        for threshhold in tqdm(confidenceThreshholdList):
            confident_confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)
            confusion_mat = np.zeros((len_labels, len_labels), dtype=np.int)

            unclassified_list = []
            true_classified_list = []
            false_classified_list = []
            suffix = "th"+str(threshhold)
            print("using suffix : "+ suffix)
            path_output = os.path.join(path_output,suffix)
            print("current path to store : "+ str(path_output))

            if not os.path.exists(path_output):
                os.makedirs(path_output)
            file_index = 0   
            #print("value of all results : " + str(all_results))
            for result in all_results:
                #print("value of result is :"+ str(result))
                filename = test_list[file_index].split(' ')[0]
#                 file_val = int(test_list[file_index].split(' ')[1])
                file_val = result[2]
                label = result[0]  # prediction
#                 print("label is :" +str(label))

#                 print("result  is :"+str(len(result)))
                conf = float(result[1][label])
#                 print ('conf', conf)
#                 print (type(file_val), type(label), type(conf))
#                 print (len(file_val), label.size, conf)
                #for conf in confidenceThreshholdList:
                if float(conf) > float(threshhold*0.01):  # Our threshold
                    if label == file_val:
                        true_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) +',' +basename(filename).split(".")[0]+ '\n'
                        true_classified_list.append(true_str)
                    else:
                        false_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) + ',' + str(labels[file_val]) +',' +basename(filename).split(".")[0]+ '\n'
                        false_classified_list.append(false_str)
                    confident_confusion_mat[label][file_val] += 1
                else:
                    unclassified_str = str(filename) + ',' + str(labels[label]) + ',' + str(conf) + ',' + str(labels[file_val])+',' +basename(filename).split(".")[0] + '\n'
                    unclassified_list.append(unclassified_str)
                confusion_mat[label][file_val] += 1
                file_index = file_index+1
                #print("index = "+ str(file_index))
                #print(test_list[file_index])

            logging.info('Writing confusion matrix and related files')

            df = pd.DataFrame(confusion_mat, index=model.labels[attr], columns=model.labels[attr])
            df.to_csv(os.path.join(path_output,'confusion_matrix_'+suffix+'.csv'))
            df = pd.DataFrame(confident_confusion_mat, index=model.labels[attr], columns=model.labels[attr])
            df.to_csv(os.path.join(path_output,'confident_confusion_matrix_'+suffix+'.csv'))
    #    df_map = pd.read_csv(MODEL_PATH_DICT['urlMapPath'], names=['id', 'ground_truth', 'tag', 'url', 'path'], header=None)

            with open(os.path.join(path_output,'true_classified_list_'+suffix+'.csv'), 'w+') as thefile:
                for item in true_classified_list:
                    thefile.write(item)

            with open(os.path.join(path_output,'false_classified_list_'+suffix+'.csv'), 'w+') as thefile:
                for item in false_classified_list:
                    thefile.write(item)

            with open(os.path.join(path_output,'unclassified_list_'+suffix+'.csv'), 'w+') as thefile:
                for item in unclassified_list:
                    thefile.write(item)
            path_output = path_output.replace(suffix, "")

            correct_predictions=0
            for index in range(0,confusion_mat.shape[0]):
                correct_predictions = confusion_mat[index][index] + correct_predictions
            accuracy = float(correct_predictions)/float(len(all_results))
            print("Accuracy is : "+ str(accuracy))
        
    return True


if __name__ == '__main__':
    #multimodel = False
    logging.getLogger().setLevel(logging.ERROR)
    parser = optparse.OptionParser()
    parser.add_option(
        '-g', '--gpu',
        help="use gpu mode",
        action='store_true', default=False)

    opts, args = parser.parse_args()
    opts.gpu= True
    model_details = MODEL_PATH_DICT
    # Initialize classifier + warm start by forward for allocation
#     if os.path.exists(model_details['predictionStorePath']) and model_details['usePreviousPredictions']:
#         opts.gpu= False
    #getting lates model file from folder
   
    list_of_files = glob.glob(model_details['modelPath']) 
    model_file = max(list_of_files, key=os.path.getctime)
    print("picking model file : "+str(model_file))
    pytorch_model = ModelClassifier(pretrained_model_file=model_file,
                                      class_labels_file=model_details['mapFilePath'], path_to_store_output = model_details['pathToStoreOutput'],gpu_mode=opts.gpu)
    #caffe_model.net.forward()
    logging.info('Initialized the classfier')
    get_conf_matrix(pytorch_model,model_details['pathToStoreOutput'],model_details['confidenceThreshholdList'],
                   model_details['predictionStorePath'],model_details['usePreviousPredictions'])
