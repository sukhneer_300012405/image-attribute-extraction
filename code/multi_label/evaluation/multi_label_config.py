import os
import pandas as pd

evaluation_for = "test"
article_type= "jeans"
# attributes = ['sleeve_style', 'sleeve_length', 'print_pattern_type', 'neck']
# attributes = ['sleeve_style','sleeve_length','length','print_pattern_type','neck','hemline', 'shape'] #for dresses
attributes =  ['fade','distress','shade']
architecture_folder_name = "res34_0.01"   
ckpt_parent_folder = '/multi_label/ckpt_unaug_data_marginloss_maxprob/'
base_folder = '/rapid_data/myntra/myntra_data/'

REPO_DIRNAME = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../..')

overall_df = pd.read_csv('/rapid_data/myntra/myntra_data/' + article_type  + '/multi_label/OHE_label.txt', sep = " ")
OHE_attributes = overall_df.columns.tolist()[1:]
OHE_attributes = [x.replace(' ','_') for x in OHE_attributes]

mapFilePath_dict = {}
predictionStorePath_dict = {}
pathToStoreOutput_dict = {}

for attr in attributes:
    mapFilePath_dict[attr] = base_folder+article_type +'/' + article_type +'_'+attr+'/'+article_type+'_'+attr + '_label_map.txt'
    predictionStorePath_dict[attr] = base_folder + article_type + '/' + article_type + '_' + attr +'/evaluation-multilabel/'+ evaluation_for + '/predictions.txt'
    pathToStoreOutput_dict[attr] = base_folder + article_type + '/' + article_type + '_' + attr + '/evaluation-multilabel/'+ evaluation_for

MODEL_PATH_DICT = dict(   
description='Fine-tuned ImageNet model for back pose',
pathToStoreOutput = pathToStoreOutput_dict,
mapFilePath = mapFilePath_dict,
modelPath = base_folder + article_type + ckpt_parent_folder + architecture_folder_name + '/*',
testFilePath = base_folder + article_type + '/multi_label/OHE_multi_label_' + evaluation_for + '.txt',  
predictionStorePath = predictionStorePath_dict,
usePreviousPredictions = False,
confidenceThreshholdList = [80,70,60,50], multimodel = False, resnet_split_layer = 6
                        )

# attribute_list = ['length'] )
#,neck, 'sleeve_length', 'print_pattern_type', 'shape', 'sleeve_style', 'length'])
