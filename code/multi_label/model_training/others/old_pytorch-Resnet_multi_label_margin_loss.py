from __future__ import print_function, division

import os
import sys
os.environ["CUDA_VISIBLE_DEVICES"]="0" 
# sys.path.append('../')
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
#import matplotlib.pyplot as plt
import time
import pandas as pd
from datetime import datetime
import ImageFileList
from torch import sigmoid

#plt.ion()   

article_type = 'dresses'
attributes = ['sleeve_style', 'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline', 'shape']
batch_size_num = 32
train_file_name = 'OHE_label_train.txt'
test_file_name = 'OHE_label_test.txt'
learning_rate = 0.01
momentum = 0.9
step_size = 4
num_epochs = 20
threshold = 0.7

data_transforms = {
    'train': transforms.Compose([
        transforms.Scale( (224,224) ),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'val': transforms.Compose([
        transforms.Scale( (224,224) ),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}

train_val_list = ['train' , 'val']
path_to_input_file = {
    'train' :'/rapid_data/myntra/myntra_data/' + article_type  + '/multi_label/' + train_file_name ,
    'val': '/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/' + test_file_name
    }
path_to_checkpoint = '/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/OHE_multilabel_threshold_' + str(threshold) + '/res34_' + str(learning_rate)
# path_to_checkpoint = '/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/ckpt_unaug_data_multilabel_margin/res34_' + str(learning_rate)

if not os.path.exists(path_to_checkpoint):
    print("creating directory for checkpoint...")
    os.makedirs(path_to_checkpoint)

image_datasets = {x: ImageFileList.ImageFilelisit_a( flist=path_to_input_file[x],
                                   transform = data_transforms[x])
                  for x in train_val_list}

dataloders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=batch_size_num,
                                             shuffle=True, num_workers=4)
              for x in train_val_list}


print ('Current time and date is: ' + str(datetime.now()) )
print ('The model is being trained for: ' +  article_type)
print ('train file is: ' + train_file_name )
dataset_sizes = {x: len(image_datasets[x]) for x in train_val_list}
print ('dataset_sizes: ' + str(dataset_sizes) )
#class_names = image_datasets['train'].classes

use_gpu = torch.cuda.is_available()
print("checking gpu")
print(use_gpu)

overall_df = pd.read_csv('/rapid_data/myntra/myntra_data/' + article_type  + '/multi_label/OHE_label.txt', sep = " ")
OHE_attributes = overall_df.columns.tolist()[1:]
OHE_attributes = [x.replace(' ','_') for x in OHE_attributes]

def train_model(model, criterion, optimizer, scheduler, num_epochs):
    since = time.time()

    best_model_wts = model.state_dict()
    best_acc = 0.0

    for epoch in range(num_epochs):
        epoch_start = time.time()
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in train_val_list:
            if phase == 'train':
                scheduler.step()
                model.train(True)  # Set model to training mode
            else:
                model.train(False)  # Set model to evaluate mode

            running_loss = 0.0
            
            running_corrects = {}
            for attrib in attributes:
                running_corrects[attrib] = 0 

            # Iterate over data.
            for data in dataloders[phase]:

                inputs, labels = data
                labels_list = []
                for l in labels:
                    l = list(l)
                    l = torch.LongTensor([int(x) for x in l])
                    l = Variable(l).cuda()
                    labels_list.append(l)
                
                OHE_attributes_dict ={}
                for i, att in enumerate(OHE_attributes):
#                     locals()[att] = labels_list[i]
                    OHE_attributes_dict[att] = labels_list[i]
    
#                 print (OHE_attributes_dict.keys())
                inputs = Variable(inputs.cuda())

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                outputs = model(inputs)
                outputs = outputs.view(outputs.size()[1], outputs.size()[0]) #transpose output to 79 * 32 dim
                preds = {}
                for i, attrib in enumerate(OHE_attributes):
                    prob = sigmoid(outputs[i])
#                     print (type(prob), prob.size(), type(prob.data[0]) )
                    pred = [1 if x >= threshold else 0 for x in prob.data] 
#                     _, pred = torch.max(outputs[i].data, 1)
                    preds[attrib] = np.array(pred)
                
#                 print (preds)
#                 loss = 0
#                 for i, l in enumerate(OHE_attributes_vars):
#                     loss += criterion(outputs[i], l) 
#                 print (type(outputs), type(OHE_attributes_vars) ) 
#                 print (len(outputs), len(OHE_attributes_vars) )
        
#                 for l in OHE_attributes_vars:
#                     l = list(l)
#                     l = torch.LongTensor([int(x) for x in l])
#                     l = Variable(l).cuda()

                for i, att in enumerate(attributes):
                    att_classes = [x for x in OHE_attributes if x.startswith(att)]
                    att_classes_list = [OHE_attributes_dict[x] for x in att_classes ]
#                     print (len(att_classes_list), type(att_classes_list[0]) )
#                     print (att_classes)
                    att_classes_pred = []
                    for l, attrib in zip(att_classes_list, att_classes):
                        att_classes_pred.append(preds[attrib])
                    att_classes_pred = np.array(att_classes_pred)
                    att_classes_pred_sum = att_classes_pred.sum(axis=0)
                    one_pred_indices = np.where(att_classes_pred_sum == 1)[0]
#                     print (att, 'pred_list:' , pred_list, '\n')
#                     print ('pred_list_sum', pred_list_sum , '\n')
#                     print('one_pred_indices:', one_pred_indices, '\n')
                    
                    if len(one_pred_indices) > 0:
                        for actual, attrib in zip(att_classes_list, att_classes):
#                             print (attrib, 'one_pred_indices:', one_pred_indices)
#                             print ('l:', len(l),  'preds[attrib]:', preds[attrib])
                            actual1 = actual.data.index([one_pred_indices])  
                            preds_one_class = preds[attrib][one_pred_indices]
#                             print ( 'l1:', len(l1), 'preds_indices:', preds_one_class)
#                             print (att, attrib, l1)
                            actual2 = [x for x in actual1 if x == 1]
                            prediction = [y for x, y in zip(actual1, preds_one_class) if x == 1]
#                             if len(pred)>0:
#                                 print (att, attrib, preds_one_class, len(l2))
#                                 print (att, attrib, pred, l2.numpy(), type(pred), type(l2.numpy()), '\n\n')
                            running_corrects[att] += np.sum(prediction == actual2)
                
                    
#                 print (type(OHE_attributes_vars), len(OHE_attributes_vars), OHE_attributes_vars[0].size())
#                 OHE_attributes_tensor = torch.stack(OHE_attributes_vars)
                OHE_attributes_tensor = torch.stack(list(OHE_attributes_dict.values()))
    
#                 print (type(OHE_attributes_tensor ) ) 
#                 print (type(outputs), OHE_attributes_vars)
#                 print (outputs.size(), OHE_attributes_tensor.size() )
                loss = criterion(outputs, OHE_attributes_tensor)

                # backward + optimize only if in training phase
                if phase == 'train':
                    loss.backward()
                    optimizer.step()

                # statistics
                running_loss += loss.data[0]
                
#                 for l, attrib in zip(list(OHE_attributes_dict.values()), OHE_attributes):
#                     temp = [ x for x in l.data if x == 1]
#                     if len(temp) > 0:
#                         l1 = Variable(torch.LongTensor(temp))
#                         pred_list = []
#                         for attrib_class in :
#                             pred = torch.LongTensor([y for x, y in zip(l.data, preds[attrib]) if x == 1])
#                         running_corrects[attrib] += torch.sum(pred == l1.data)
                    
#                 for l, attrib in zip(list(OHE_attributes_dict.values()), OHE_attributes):
#                     temp = [ x for x in l.data if x == 1]
#                     if len(temp) > 0:
#                         l1 = Variable(torch.LongTensor(temp))
#                         pred = torch.LongTensor([y for x, y in zip(l.data, preds[attrib]) if x == 1])
#                         running_corrects[attrib] += torch.sum(pred == l1.data)
#                         running_corrects[attrib] += len(l1)
#                     print (attrib, running_corrects[attrib], 'dataset_sizes[phase]:' , dataset_sizes[phase])

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = {}
                
            for i, att in enumerate(attributes):
                locals()[att + '_classes'] = [x for x in OHE_attributes if x.startswith(att)]
#                 print ('\n', i , locals()[att + '_classes'], len(locals()[att + '_classes']))
                
            for attrib in attributes:
#                 attrib_classes = locals()[attrib + '_classes']
                epoch_acc[attrib] = running_corrects[attrib] / dataset_sizes[phase]
                print('{} Accuracy of {} is: {:.4f}'.format(phase, attrib, epoch_acc[attrib]) )
                  
            overall_acc = sum(epoch_acc.values())/len(epoch_acc)          

            print('\n{} Loss: {:.4f} Overall Accuracy : {:.4f}\n'.format(
                phase, epoch_loss, overall_acc))

            # deep copy the model
            if phase == 'val' and overall_acc > best_acc:
                best_acc = overall_acc
                best_model_wts = model.state_dict()
                save_checkpoint(epoch,{
                    'epoch': epoch + 1,
                    'state_dict': model.state_dict(),
                    'best_acc': overall_acc,
                    'optimizer' : optimizer.state_dict(),
                })
        epoch_end = time.time() - epoch_start
        print('{}th epoch training completed in {:.0f}m {:.0f}s'.format(epoch,
        epoch_end // 60, epoch_end % 60))
        print()

    time_elapsed = time.time() - since
    print('Entire Training completed in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model


def save_checkpoint(epoch, state, path_to_checkpoint = path_to_checkpoint, filename = 'batch'+ str(batch_size_num) + '_ckpt.pth.tar'):
    filepath = os.path.join(path_to_checkpoint, "epoch_" + str(epoch) + "_" + filename)
    torch.save(state, filepath)
    
    
def load_checkpoint(filepath=None):
    if filepath:
        if os.path.isfile(filepath):
            print("=> loading checkpoint '{}'".format(filepath))
            checkpoint = torch.load(filepath)
            args.start_epoch = checkpoint['epoch']
            best_acc = checkpoint['best_acc']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(filepath))

# number of classes in train dataset
df = pd.read_csv(path_to_input_file['train'], header = None, sep =' ')
df.columns = overall_df.columns
# n_classes_sleeve_style = df['sleeve_style_label'].nunique()
# n_classes_sleeve_length = df['sleeve_length_label'].nunique()
# n_classes_length = df['length_label'].nunique()
# n_classes_print = df['print_label'].nunique()
# n_classes_neck = df['neck_label'].nunique()
# n_classes_hemline = df['hemline_label'].nunique()
# n_classes_shape = df['shape_label'].nunique()

print ('using resnet34 model architecture...')
from torchvision.models.resnet import BasicBlock

model_ft = models.resnet34(pretrained=True)
num_ftrs = model_ft.fc.in_features
del model_ft

NUM_CLASSES = 79
FIRST_FC_NODES = 1024

class MyResnet2(models.ResNet):
    def __init__(self, block, layers, num_classes = 1000):
        super(MyResnet2, self).__init__(block, layers, num_classes)
        
        model_ft = models.resnet34(pretrained=True)
        model_state_dict = model_ft.state_dict()
        self.load_state_dict(model_state_dict)
        
        self.fc = nn.Linear(num_ftrs, FIRST_FC_NODES)
        self.fc1 = nn.Linear(FIRST_FC_NODES , NUM_CLASSES)

        
    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x) 
        x = self.layer4(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        x = self.fc1(x)
    
        return x

model_ft = MyResnet2(BasicBlock, [3, 4, 6, 3], 1000)

if use_gpu:
    model_ft = model_ft.cuda()

criterion = nn.MultiLabelMarginLoss()
optimizer_ft = optim.SGD(model_ft.parameters(), lr=learning_rate, momentum=momentum)
# Observe that all parameters are being optimized

# Decay LR by a factor of 0.1 every 6 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=step_size, gamma=0.1)


model_ft = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler,
                       num_epochs=num_epochs)
