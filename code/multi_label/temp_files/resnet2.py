import os
os.environ["CUDA_VISIBLE_DEVICES"]="0" 
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
#import matplotlib.pyplot as plt
import time
import pandas as pd
from datetime import datetime
import json
import operator
from os.path import basename
import pickle 
from torch import sigmoid

from tqdm import tqdm
from PIL import Image
import time
import logging
import optparse
import numpy as np
import pandas as pd
from multi_label_config import *
import glob
import os
import ImageFileList
from torchvision.models.resnet import BasicBlock

class MyResnet2(models.ResNet):
    def __init__(self, block, layers, num_classes = 1000):
        super(MyResnet2, self).__init__(block, layers, num_classes)

        model_ft = models.resnet34(pretrained=True)
        model_state_dict = model_ft.state_dict()
        self.load_state_dict(model_state_dict)

        self.fc = nn.Linear(num_ftrs, FIRST_FC_NODES)
        self.fc1 = nn.Linear(FIRST_FC_NODES , NUM_CLASSES)


    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x) 
        x = self.layer4(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        x = self.fc1(x)

        return x