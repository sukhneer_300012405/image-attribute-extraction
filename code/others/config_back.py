__author__ = '8372'

import os

REPO_DIRNAME = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../..')
MODEL_PATH_DICT = dict(description='Fine-tuned ImageNet model for back pose',
			   deployPath='/home/myntra/caffe-models/dresses/myntra_dresses_all_pose_aug/deploy.prototxt',
			   modelPath='/home/myntra/caffe-models/dresses/myntra_dresses_all_pose_aug/myntra_dresses_all_pose_aug_iter_150000.caffemodel',
                           meanFilePath='/home/myntra/installation_files/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy',
                           mapFilePath='/data1/myntra_dresses/map/dresses_all_pose_map.txt',
                           fileMapPath='/data1/scripts/machinevision/data_scripts/temp/dresses_data_21_7_16/cms_data_79_kids_filemap.csv')


