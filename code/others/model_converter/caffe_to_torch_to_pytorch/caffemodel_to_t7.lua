require 'loadcaffe'
require 'xlua'
require 'optim'

—- modify the path 

prototxt = '/rapid_data/myntra/myntra_data/code/pose_model/topwear/deploy.prototxt'
binary = '/rapid_data/myntra/myntra_data/code/pose_model/topwear/snapshot_iter_34600.caffemodel'

net = loadcaffe.load(prototxt, binary, 'cudnn')
net = net:float() —- essential reference https://github.com/clcarwin/convert_torch_to_pytorch/issues/8
print(net)

torch.save('/rapid_data/myntra/myntra_data/code/pose_model/topwear/vgg16_torch.t7', net)




