__author__ = '8372'

import time
import logging
import optparse
import numpy as np
import pandas as pd
from config_model import *
#from config_bottomwear import *
import exifutil
import os
#os.environ["CUDA_VISIBLE_DEVICES"]="0" 
import caffe


# TO DO: Check if all file exists, else don't let the model through

class ModelClassifier(object):
    def __init__(self, model_def_file, pretrained_model_file, mean_file,
                 raw_scale, class_labels_file, image_dim, gpu_mode,is_pixel_mean=True):
        logging.info('Loading net and associated files...')
        if gpu_mode:
            caffe.set_mode_gpu()
            caffe.set_device(0)
            print ('GPU mode is ON')
        else:
            caffe.set_mode_cpu()
            print ('GPU mode is OFF')
        self.net = caffe.Classifier(
            model_def_file, pretrained_model_file,
            image_dims=(image_dim, image_dim), raw_scale=raw_scale,
            #mean=np.load(mean_file).mean(1).mean(1), 
	    channel_swap=(2, 1, 0)
        )
	print("Initialize")
	if mean_file is not None:
            self.__set_mean((image_dim, image_dim), mean_file, is_pixel_mean)

        with open(class_labels_file) as f:
            labels_df = pd.DataFrame([
                                         {
                                             'id': l.strip().split(' ')[0][1:],
                                             # The [1:] to get rid of 'n' in labels of the format n<label_id>
                                             'name': ' '.join(l.strip().split(' ')[1:]).split(',')[0]
                                         }
                                         for l in f.readlines()
                                         ])
            labels_df = labels_df.convert_objects(convert_numeric=True)
            print (labels_df.head() )
        self.labels = labels_df.sort_values('id')['name'].values

    def __set_mean(self, image_dims, mean_file, is_pixel_mean):

        if is_pixel_mean:
            _mean = np.load(mean_file).mean(1).mean(1)
        else:
            # Do a center crop on the mean
            center = np.array(image_dims) / 2.0
            print (image_dims)
            crop_dims = np.asanyarray(self.net.crop_dims)
            crop = np.tile(center, (1, 2))[0] + np.concatenate([
                -crop_dims / 2.0,
                crop_dims / 2.0
            ])
            crop = crop.astype(int)
            _mean = np.load(mean_file)[:, crop[0]:crop[2], crop[1]:crop[3]]

        self.net.transformer.set_mean(self.net.inputs[0], _mean)
        return

    def classify_image(self, image):
        try:
            starttime = time.time()
            scores = self.net.predict([image], oversample=False).flatten()
            endtime = time.time()
            indices = (-scores).argsort()[:1]
            predictions = self.labels[indices]

            # In addition to the prediction text, we will also produce
            # the length for the progress bar visualization.
            meta = [
                (p, '%.5f' % scores[i])
                for i, p in zip(indices, predictions)
                ]
            logging.info('result: %s', str(meta))
#            print (endtime-starttime)

            return True, meta, '%.3f' % (endtime - starttime)

        except Exception as err:
            logging.info('Classification error: %s', err)
            return (False, 'Something went wrong when classifying the '
                           'image. Maybe try another one?')


def get_conf_matrix(model):

    labels = model.labels.tolist()
    print (str(labels) + str(len(labels)) )
    len_labels = len(labels)
    unclassified_list = []
    true_classified_list = []
    false_classified_list = []

    test_file_path = MODEL_PATH_DICT['testFilePath']
    print('test_file_path: ' + test_file_path)
    test_file_dir = os.path.dirname(test_file_path)
    print ('test_file_dir: ' + str(test_file_dir) )
    with open(test_file_path) as f:
        test_list = f.readlines()
    test_list = [x.split(' ')[0] for x in test_list]
    print ('test_list sample rows: ' +  str(test_list[:3]) )
    tracker = 0
    for test_line in test_list:
        
        tracker += 1
        logging.info(str(tracker)+'/'+str(len(test_list)))
        try:
            filename = test_line.strip('\n').strip('\r')
            image = exifutil.open_oriented_im(filename)
        except Exception as e:
            print (e)
            print ("ERROR is in :" + filename)
            continue

        result = model.classify_image(image)
        if result[0]:  # Status
            top_result = result[1][0]
            label = top_result[0]
            conf = float(top_result[1])
            true_str = str(filename)+ ',' + label + ',' + str(conf) + '\n'
            true_classified_list.append(true_str)

    with open(os.path.join(test_file_dir, 'dresses_length_train_pose.txt'), 'w+') as thefile:
        for item in true_classified_list:
            thefile.write(item)
 
    return True


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    parser = optparse.OptionParser()
    parser.add_option(
        '-g', '--gpu',
        help="use gpu mode",
        action='store_true', default=False)

    opts, args = parser.parse_args()

    model_details = MODEL_PATH_DICT
    # Initialize classifier + warm start by forward for allocation
    caffe_model = ModelClassifier(model_def_file=model_details['deployPath'],
                                  pretrained_model_file=model_details['modelPath'],
                                  mean_file=model_details['meanFilePath'], raw_scale=255.,
                                  class_labels_file=model_details['mapFilePath'], image_dim=256, gpu_mode=opts.gpu,is_pixel_mean=False)
    print("here")
    caffe_model.net.forward()
    logging.info('Initialized the classfier')
    get_conf_matrix(caffe_model)
