__author__ = '8372'

import os

REPO_DIRNAME = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../..')

MODEL_PATH_DICT = dict(description='Fine-tuned ImageNet model for back pose',
			   deployPath='/rapid_data/myntra/myntra_data/code/pose_model/topwear/deploy.prototxt',
			   modelPath='/rapid_data/myntra/myntra_data/code/pose_model/topwear/snapshot_iter_34600.caffemodel',
                           meanFilePath='/rapid_data/myntra/myntra_data/code/pose_model/topwear/mean.npy',
                           mapFilePath='/rapid_data/myntra/myntra_data/code/pose_model/topwear/pose_map.txt',         
                          testFilePath='/rapid_data/myntra/myntra_data/dresses_all_poses/dresses_length/dresses_length_train.txt')
