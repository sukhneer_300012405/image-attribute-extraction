
# coding: utf-8

# In[107]:

import warnings
warnings.filterwarnings('ignore')


# In[108]:

import numpy as np
import pandas as pd
import os
import time
import urllib
import multiprocessing
import requests
import math

# In[109]:

article_type = 'tops'
image_dir = '/rapid_data/myntra/poshaq_data/' + article_type + '/' + article_type + '_images'
presto_file_path = '/rapid_data/myntra/poshaq_data/' + article_type
print ('image_dir: ' + image_dir +  '\n' + 'presto_file_path: ' + presto_file_path)

if not os.path.exists(image_dir):
    os.makedirs(image_dir)

# In[110]:

#df = pd.read_csv( presto_file_path + '/' + article_type + '.csv', header= None, delimiter = "\t")
df = pd.read_csv( presto_file_path + '/Poshaq_' + article_type + '.csv',  delimiter = "\t")

#for dresses
#df.columns = ['style_id', 'brand', 'article_type', 'gender', 'base_colour',  'image_url', 'sleeve_style', 'sleeve_length', \
#'length', 'print_pattern_type', 'neck', 'hemline', 'shape']
#for shirts
#df.columns = ['style_id', 'brand', 'article_type', 'gender', 'base_colour',  'image_url',  'sleeve_length', \
#'length', 'print_pattern_type', 'neck', 'hemline']

#for jeans
#df.columns = ['style_id', 'brand', 'article_type', 'gender', 'base_colour','image_array',  'image_url',  'fade', \
#'length', 'distress', 'shade']
#df.columns = ['style_id','origin','brand','article_type','gender','product_url','image_url','image1','image2','image3','image4','source','style_id2','neck','sleeve_length','length','hemline','print_pattern_type','colour','rank']

df.columns = ['style_id','origin','brand','article_type','gender','product_url','image_url','image1','image2','image3','image4','source','style_id2','neck','sleeve_length','type','print_pattern_type','sleeve_style','length','colour','rank']

print(df.head())

df.style_id2 = df.style_id.astype(str)
print (article_type + ' dataframe shape : ' + str(df.shape))

# In[111]:

########################################################################
class MultiProcDownloader(object):
    """
    Downloads urls with Python's multiprocessing module
    """

    #----------------------------------------------------------------------
    def __init__(self, urls):
        """ Initialize class with list of urls """
        self.urls = urls

    #----------------------------------------------------------------------
    def run(self):
        """
        Download the urls and waits for the processes to finish
        """
        jobs = []
        cores = multiprocessing.cpu_count()
        batches = np.array_split(urls, cores)

        for i, data in enumerate(batches):
            print ('Size of {}th batch is '.format(i) + str(len(data)) )
            process = multiprocessing.Process(target=self.worker, args=(data, ))
            jobs.append(process)
            process.start()         

        for job in jobs:
            job.join()    
            
    #----------------------------------------------------------------------
    def worker(self, data):
        """
        The target method that the process uses to download the specified url
        """
        for i in range(len(data)):

            style_id, url = data[i,0], data[i,1]
            msg = "Starting download of %s" % style_id
            if i % int(len(data) * 0.25) == 0 and i > 0:
                print(i * 100 / len(data), '% images assigned to a core downloaded')
            fname = image_dir  + '/' + str(style_id).replace('/', '-') + '.jpg'          
            try:
                r = requests.get(url)
                with open(fname, "wb") as f:
                    f.write(r.content)
            except Exception as e:
                print(style_id, url, '\n', e)


# In[112]:

start_time = time.time()
if __name__ == "__main__":
    urls = df[['style_id2', 'image_url']].values
    #urls= df['image_url']
    downloader = MultiProcDownloader(urls)
    downloader.run()
print ('Time taken for downloading all Images: ', time.time()-start_time)


# ### Deleting junk image files

# In[119]:

print ('\n\n\n Deleting junk image files')

import os
from PIL import Image
total_count = 0
deleted_styles = []
valid_styles = []
for image in os.listdir(image_dir):
    total_count += 1
    path_to_image = os.path.join(image_dir,image)
    style_id = image.split('.')[0]
    
    try:
        Image.open(path_to_image)
        valid_styles.append(style_id)
    except Exception as e:
#         print("exception as : " + str(e))
        print("deleting image :" + str(image))
        os.remove(path_to_image)
        deleted_styles.append(style_id)


# In[113]:

print ("total image URLs : " ,  len(df))
print ("total downloaded images : " ,  total_count)
print ("total deleted images : " ,   len(deleted_styles) )
print ("total valid images : " ,   len(valid_styles) )


# In[117]:

df_valid = df[df.style_id.isin(valid_styles)]
print ('\npresto df shape ' +  str(df.shape))
print ('valid df shape ' +  str(df_valid.shape))
print ('writing final file having only valid style ids')
df_valid.to_csv(presto_file_path + '/' +  article_type +  '_valid_styles.csv', index = False, header= None)
