import warnings
warnings.filterwarnings('ignore')
import numpy as np
import pandas as pd
import os
import time
import urllib
import multiprocessing
import requests
import math
from tqdm import tqdm

article_type = 'dresses'
image_dir = '/data1/image_attributes/data/myntra_data/' + article_type + '_all_poses/images'
presto_file_path = '/data1/image_attributes/data/myntra_data/' + article_type + '_all_poses'
print (image_dir +  '\n' + presto_file_path)

print('Reading presto file...')
presto_df = pd.read_csv( presto_file_path + '/' + article_type + '_all_poses.csv', header= None)
presto_df.columns = ['style_id', 'brand', 'article_type', 'gender', 'base_colour', 'image_url_array',  'image_url', 'sleeve_style', 'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline', 'shape']
presto_df.style_id = presto_df.style_id.astype(str)
print (article_type + ' dataframe shape : ' + str(presto_df.shape))

cores = multiprocessing.cpu_count()
def parallelize_dataframe(df, func):
    df_split = np.array_split(df, cores)
    pool = multiprocessing.Pool(cores)
    df = pd.concat(pool.map(func, df_split))
    pool.close()
    pool.join()
    return df

def get_all_urls(array_df):
    all_urls_df = pd.DataFrame()
    for style_id, image_array, front_image in tqdm(zip(array_df.style_id, array_df.image_url_array, array_df.image_url)):
        image_array = image_array.replace('"', '').replace('[', '').replace(']', '')
        images = image_array.split(',')
        images.append(front_image)
        images = list(set(images))
        new_style_ids = [style_id + '_' + str(x) for x in np.arange(len(images))]
        temp = pd.DataFrame({'style_id': style_id,'style_id_appended': new_style_ids, 'image_url' : images })
        temp = temp[temp.image_url != 'null']
        all_urls_df = all_urls_df.append(temp)
    return all_urls_df
    
print('Creating dataframe having all image URLs ...')
images_df = parallelize_dataframe(presto_df,get_all_urls)
images_df = images_df.merge(presto_df.drop(['image_url', 'image_url_array'], axis = 1), how = 'left', on = 'style_id')
print('Creation of dataframe having all image URLs completed ...')
print (article_type + ': Total Images to download : ' + str(len(images_df)))

########################################################################

class MultiProcDownloader(object):
    """
    Downloads urls with Python's multiprocessing module
    """

    #----------------------------------------------------------------------
    def __init__(self, urls):
        """ Initialize class with list of urls """
        self.urls = urls

    #----------------------------------------------------------------------
    def run(self):
        """
        Download the urls and waits for the processes to finish
        """
        jobs = []
        cores = multiprocessing.cpu_count()
        batches = np.array_split(urls, cores)

        for i, data in enumerate(batches):
            print ('Size of {}th batch is '.format(i) + str(len(data)) )
            process = multiprocessing.Process(target=self.worker, args=(data, ))
            jobs.append(process)
            process.start()         

        for job in jobs:
            job.join()    
            
    #----------------------------------------------------------------------
    def worker(self, data):
        """
        The target method that the process uses to download the specified url
        """
        for i in range(len(data)):

            style_id, url = data[i,0], data[i,1]
            msg = "Starting download of %s" % style_id
            if i % int(len(data) * 0.2) == 0 and i > 0:
                print i * 100 / len(data), '% images assigned to a core downloaded'
            fname = image_dir  + '/' + str(style_id) + '.jpg'          
            try:
                r = requests.get(url)
                with open(fname, "wb") as f:
                    f.write(r.content)
            except Exception as e:
                print style_id, url, '\n', e


start_time = time.time()
if __name__ == "__main__":
    urls = images_df[['style_id_appended', 'image_url']].values
    print ('downloading all Images...')
    downloader = MultiProcDownloader(urls)
    downloader.run()
print ('Time taken for downloading all Images: ', time.time()-start_time)


print ('\n\nDeleting junk image files')

import os
from PIL import Image
total_count = 0
deleted_styles = []
valid_styles = []
for image in os.listdir(image_dir):
    total_count += 1
    path_to_image = os.path.join(image_dir,image)
    style_id = image.split('.')[0]
    
    try:
        Image.open(path_to_image)
        valid_styles.append(style_id)
    except Exception as e:
        print("deleting image :" + str(image))
        os.remove(path_to_image)
        deleted_styles.append(style_id)


print ("total images to download : " ,  len(images_df))
print ("total downloaded images : " ,  total_count)
print ("total deleted images : " ,   len(deleted_styles) )
print ("total valid images : " ,   len(valid_styles) )


df_valid = images_df[images_df.style_id_appended.isin(valid_styles)]
print ('\npresto df shape ' +  str(presto_df.shape))
print ('valid df shape ' +  str(df_valid.shape))
print ('writing final file having only valid style ids')
df_valid.to_csv(presto_file_path + '/' +  article_type +  '_allposes_valid_styles.csv', index = False, header= None)
