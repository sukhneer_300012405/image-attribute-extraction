from pyhive import presto
import time
import os
import re
import sys
import traceback
import datetime
import csv, codecs, cStringIO

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        try:

#             def clean_value(x):
#                 try:
#                     x = x.replace("\n", " ").encode("utf-8")
#                 except Exception as e:
#                     x = "NA"
#                 return x

            def clean_value(x):
                if (x is None):
                    x = 'NA'
                x = x.replace("\n", " ").encode("utf-8")
                return x
                    
            self.writer.writerow([clean_value(s) for s in row])
            
        except AttributeError as e:
            print(e)
            #None
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


db_user = "hadoop"
db_pwd = ""
db_host = "rapid-analytics.mynt.myntra.com"
db_port = 8889
db_connections = {}

def u_encode(input):                                                                                  
    if isinstance(input, dict):                                                                       
        return {u_encode(key):u_encode(value) for key,value in input.iteritems()}                     
    elif isinstance(input, list):                                                                     
        return [u_encode(element) for element in input]                                               
    elif isinstance(input, unicode):                                                                  
        return input.encode('utf-8')  
    else:                                                                                             
        return input

def execute_query_and_write_output(str, source='output'):
    conn = get_pg_conn()
    cursor = conn.cursor()
    cursor.execute(str)
    #f = open('/scratch4/text_attrib/data/all_source/dresses/%s.csv' % source, 'w')
    filename ='/data2/image_attributes/data/myntra_data/kurtas/kurtas_all_poses.csv'
    #filename ='/data2/image_attributes/temp/dresses_tops_kurta.csv'
    FILE = open(filename,"w");
    output = UnicodeWriter(FILE)

    for row in cursor: 
        output.writerow(u_encode(row))
    cursor.close()
    FILE.close()

def get_pg_conn():
    pid = str(os.getpid())
    conn = None
    # get the database connection for this PID
    try:
        conn = db_connections[pid]
    except KeyError:
        pass

    if conn == None:
        try:
            conn = presto.connect(host=db_host, port=db_port, username=db_user)
        except Exception as e:
            write(e)
            write('Unable to connect to Cluster Endpoint')
            cleanup()
            sys.exit(ERROR)
        db_connections[pid] = conn
    return conn


if __name__ == "__main__":
    
    sql_query = '''
	select style_id, brand, article_type, gender, base_colour, image_array, image, sleeve_style, sleeve_length, length, 
	print_pattern_type, neck, hemline,  shape
        from (
        select style_id, brand, article_type, gender, base_colour, image_array, image,
        json_extract_scalar(product_attributes, '$["Sleeve Styling"]') as sleeve_style,
        json_extract_scalar(product_attributes, '$["Sleeve Length"]') as sleeve_length, 
        json_extract_scalar(product_attributes, '$.Length') as length,
        json_extract_scalar(product_attributes, '$["Print or Pattern Type"]') as print_pattern_type,  
        json_extract_scalar(product_attributes, '$.Neck') as neck,
        json_extract_scalar(product_attributes, '$.Hemline') as hemline,
        json_extract_scalar(product_attributes, '$.Shape') as shape
        from rapid.rapid_dim_style where origin = 'CMS'
        and article_type in ('Kurtas')
        and gender in ('Women')
         ) A 
        where (image <> '' and image is not null and image <> 'none')
    '''

    execute_query_and_write_output(sql_query)
