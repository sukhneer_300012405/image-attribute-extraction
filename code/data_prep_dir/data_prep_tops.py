import os
import warnings
warnings.filterwarnings('ignore')
import glob
import pandas as pd
import numpy as np
import argparse
import json
import random

article_type = 'tops'
parser = argparse.ArgumentParser()

parser.add_argument('--input_file_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type +  '_valid_styles.csv')
parser.add_argument('--image_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type+   '_images')
parser.add_argument('--attribute_map_file', default= '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type + '_' + 'attribute_map.txt' ,type=str)
parser.add_argument('--attributes', default =['sleeve_style','sleeve_length','print_pattern_type','neck'], type = str)
parser.add_argument('--output_path', default= '/rapid_data/myntra/myntra_data/' + article_type  ,type=str)
# parser.add_argument('--label_map_file_name', default = None, type = str)
parser.add_argument('--overall_map_file_name', default = article_type +  "_multilabel_overall_map_file.csv" ,type = str)
parser.add_argument('--split_ratio', default = 0.8 ,type = float)
parser.add_argument('--ignore_threshold', default = 20 ,type = int)
parser.add_argument('--keep_threshold', default = 100000 ,type = int)
# parser.add_argument('--ignore_overall_df_threshold', default = 5000 ,type = int)

def main(args):
    data = pd.read_csv(args.input_file_path)
    
    data.columns = ['style_id',  'brand', 'article_type','gender', 'color', 'image_url', \
                    'sleeve_style', 'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline', 'shape']
    data.drop(['hemline', 'shape'], axis = 1 , inplace = True)
    print ('pre:' + str(data.shape) )
    data = data.dropna()
    print ('post:' + str(data.shape) )
#     keep_rows = np.random.choice(data.index, 51000, replace=False)
#     data = data.loc[keep_rows]
    
    
    print("data read..")
    if os.path.exists(args.attribute_map_file):
        mappers = {}
        with open(args.attribute_map_file) as json_data:
            print("reading map file...")
            mappers = json.load(json_data)
            data = data.replace(mappers)
            
    data["local_image_path"] = args.image_path + "/" + data["style_id"].map(str) + ".jpg"
#     data.to_csv(os.path.join(args.output_path, args.overall_map_file_name))
    print("overall csv map file created")

    attributes = args.attributes
    ignore_threshold = args.ignore_threshold
    keep_threshold = args.keep_threshold
    
    for attribute in attributes:
        print ('Filtering the classes having count less than threshold value...')
        label_count = pd.DataFrame(data[attribute].value_counts()).reset_index()
        unique_val = data[attribute].unique().tolist()  
        print (attribute, 'label_count pre:', label_count)
#         print  ('\nUnique ' + attribute + ' values before removing rare classes:' +  str(unique_val) )
        
        label_count = label_count[label_count[attribute] > ignore_threshold]       
        data = data[data[attribute].isin(label_count['index'])]
        unique_val = data[attribute].unique().tolist()
        print (attribute, 'label_count post:', label_count)
#         print  ('\nUnique ' + attribute + ' values after removing rare classes:' +  str(unique_val) )
        
        unique_classes = data[attribute].unique().tolist()
        
        print ('Capping the classes having count greater than threshold value...')
        for classes in unique_classes:
            random.seed(1)
            total_class_rows = sum(data[attribute] == classes)
            if total_class_rows > keep_threshold:
                extra_rows = total_class_rows - keep_threshold
                print (extra_rows , len(list( data[data[attribute] == classes].index) ))
                drop_rows = random.sample( list(data[data[attribute] == classes].index), extra_rows )
                data = data.drop(drop_rows)
                
        label_count = pd.DataFrame(data[attribute].value_counts()).reset_index()
        print (attribute, 'label_count post:', label_count)
#         print  ('\nUnique ' + attribute + ' values after capping:' +  str(unique_val)  )
        
    # creating train and test rows indices
    split = args.split_ratio
    random.seed(0)
    train_rows = np.random.choice(data.index, int(len(data)* split), replace=False)
    test_rows = [x for x in data.index if x not in train_rows]
        
        
    for attribute in attributes:   
        directory = os.path.join(args.output_path, article_type + '_' + attribute)
        if not os.path.exists(directory):
            print("creating directory for attribute :\n"+ attribute)
            os.makedirs(directory)

        overall_df = data[['style_id', 'image_url', 'local_image_path', attribute]]
        label_count = pd.DataFrame(overall_df[attribute].value_counts()).reset_index()
        unique_val = overall_df[attribute].unique().tolist()
        print  ('\nUnique ' + attribute + ' Values:' +  str(unique_val) )
        
        
        labels_dict = {i:j for j,i in enumerate(unique_val)}
#         print ('\n' + str(labels_dict))
        print ('\nWriting label map file')
        with open(directory + '/' + article_type + '_' + attribute + '_label_map_with_len.txt', 'w') as file:
            file.write(json.dumps(labels_dict))
            
        df_train = overall_df.loc[train_rows]
        df_test = overall_df.loc[test_rows]
        df_train[attribute] = df_train[attribute].map(labels_dict)
        df_test[attribute] = df_test[attribute].map(labels_dict)
        df_train = df_train[['local_image_path', attribute]]
        df_test = df_test[['local_image_path', attribute]]
        
        print (attribute + '_train_nunique ' + str (df_train[attribute].unique()) )
        print (attribute + '_test_nunique ' + str(df_test[attribute].unique()))

        print  ('\nSaving final multilabel file' )
#         df_train.to_csv(directory + '/' + article_type + '_' + attribute+ '_'  + 'new_train.txt', sep=' ', index = False, header=False)
#         df_test.to_csv(directory + '/' + article_type + '_' + attribute+ '_' + 'new_test.txt', sep=' ', index = False, header=False)
        
    ############# Multi-label file preparation
    
    cols_to_keep = ['local_image_path'] + attributes
    multilabel_df = data[cols_to_keep]
    df_train = multilabel_df.loc[train_rows]
    df_test = multilabel_df.loc[test_rows]
    
#     print ('multilabel_df data set\n')
#     for col in [x for x in multilabel_df.columns if x != 'local_image_path']:
#         print (col + ':' +  str (multilabel_df[col].value_counts(normalize = False) ) )

#     print ('Train data set\n')
#     for col in [x for x in df_train.columns if x != 'local_image_path']:
#         print (col + ': ' +  str (df_train[col].nunique() ) )
#         print (col + ':' +  str (df_train[col].value_counts(normalize = True) ) )
        
#     print ('Test data set\n')
#     for col in [x for x in df_test.columns if x != 'local_image_path']:
#         print (col + ': ' +  str (df_test[col].nunique() ))
#         print (col + ':' +  str (df_test[col].value_counts(normalize = True) ) )
        
    for attribute in attributes:
        label_map_file = args.output_path + '/' + article_type + '_' + attribute + '/' + \
        article_type + '_' + attribute +  '_label_map_with_len.txt'
        with open(label_map_file) as json_data:
            labels_dict = json.load(json_data) 
#         print (attribute, labels_dict)
        df_train[attribute] = df_train[attribute].map(labels_dict)
        df_test[attribute] = df_test[attribute].map(labels_dict)
        
    multi_label_dir = args.output_path + '/'+ 'multi_label'
    if not os.path.exists(directory):
        print("creating directory")  
        os.makedirs(multi_label_dir)

    
#     df_train.to_csv(multi_label_dir + '/multi_label_train.txt', sep=' ', index = False, header=False)
#     df_test.to_csv(multi_label_dir + '/multi_label_test.txt', sep=' ', index = False, header=False)
        
    ############# Multi-label OHE file preparation  
        
    multilabel_OHE_df = pd.get_dummies(multilabel_df, columns = attributes)
#     multilabel_OHE_df.to_csv(multi_label_dir + '/OHE_label.txt', sep= ' ', index = False)
    df_train = multilabel_OHE_df.loc[train_rows]
    df_test = multilabel_OHE_df.loc[test_rows]

    print  ('\nSaving final multilabel file' )
#     df_train.to_csv(multi_label_dir + '/OHE_multi_label_train.txt', sep=' ', index = False, header=False)
#     df_test.to_csv(multi_label_dir + '/OHE_multi_label_test.txt', sep=' ', index = False, header=False)
 

if __name__ == '__main__':
    args = parser.parse_args()
    main(args)