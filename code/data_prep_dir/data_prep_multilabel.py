import os
import glob
import pandas as pd
import numpy as np
import argparse
import json
import random

article_type = 'dresses'
parser = argparse.ArgumentParser()
parser.add_argument('--input_file_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type +  '_valid_styles.csv')
parser.add_argument('--image_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type+   '_images')
parser.add_argument('--attribute_map_file', default= '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type + '_' + 'attribute_map.txt' ,type=str)
parser.add_argument('--attributes', default =['sleeve_style', 'sleeve_length','length', 'print_pattern_type', 'neck', 'hemline', 'shape'], type = str)
parser.add_argument('--output_path', default= '/rapid_data/myntra/myntra_data/' + article_type  ,type=str)
# parser.add_argument('--label_map_file_name', default = None, type = str)
parser.add_argument('--overall_map_file_name', default = article_type +  "_multilabel_overall_map_file.csv" ,type = str)

parser.add_argument('--train_file_suffix', default = "multilabel_train", type = str)
parser.add_argument('--test_file_suffix', default = "multilabel_test", type = str)

def main(args):
    data = pd.read_csv(args.input_file_path)
    
    data.columns = ['style_id',  'brand', 'article_type','gender', 'color', 'image_url', \
    'sleeve_style', 'sleeve_length', 'length' , 'print_pattern_type', 'neck' ,  'hemline', 'shape' ]
    
#         data1.columns = ['style_id', 'brand', 'article_type','gender', 'color', 'image_url', 'sleeve_style', 'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline', 'shape']
    data = data.dropna()

    print("data read..")
    if os.path.exists(args.attribute_map_file):
        mappers = {}
        with open(args.attribute_map_file) as json_data:
            print("reading map file...")
            mappers = json.load(json_data)
            data = data.replace(mappers)
            
    data["local_image_path"] = args.image_path + "/" + data["style_id"].map(str) + ".jpg"
#     data.to_csv(os.path.join(args.output_path, args.overall_map_file_name))
    print("overall map file created")
    
    attributes = args.attributes
    print ('\nattributes:\n' + str(attributes))   
    overall_df = data.copy()
    
    for attribute in attributes:
        print (attribute, overall_df.shape)
        overall_df = overall_df[~overall_df[attribute].isnull()]
        print (attribute, overall_df.shape)
#         overall_df = overall_df[['style_id', 'image_url', 'local_image_path', attribute]]
#         print ('\n' + attribute + ' dataframe columns:\n'  + str(overall_df.columns) )
        
        label_map_file = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type + '_' + attribute + '/' + \
        article_type + '_' + attribute +  '_label_map.txt'
        
        with open(label_map_file) as json_data:
            labels_dict = json.load(json_data) 
            
        unique_val = []
        for key in labels_dict.keys():
            unique_val.append(key)
        print (attribute)
#         print ('Unique values in old data: ' + str(len(unique_val)) )
#         print ('Pre shape of the data frame' + str(overall_df.shape) )
#         print ('Pre unique values: ' + str(len(overall_df[attribute].unique().tolist())) )
        overall_df = overall_df[overall_df[attribute].isin(unique_val)]
#         print ('Post shape of the data frame' + str(overall_df.shape) )
#         print ('Pre unique values: ' + str(len(overall_df[attribute].unique().tolist())) )
        
        overall_df[attribute] = overall_df[attribute].map(labels_dict)
        
#         df_test[attribute] = df_test[attribute].map(labels_dict)
    cols_to_keep = ['local_image_path'] + attributes
    overall_df = overall_df[cols_to_keep]
    df_train = pd.DataFrame()
    df_test = pd.DataFrame()
    split = 0.8
    print ('overall_df.shape' , overall_df.shape )
    print ('\nSplitting data into train and test...')
    unique_prints = overall_df['print_pattern_type'].unique()
#     print ('unique_prints' + str(unique_prints) )
#     print ( overall_df['print_pattern_type'].value_counts() )
    for val in unique_prints:
#         print('val:' , val)
        temp = overall_df[overall_df['print_pattern_type'] == val].reset_index(drop = True)
        np.random.seed(1)
        train_rows = np.random.choice(temp.index, int(len(temp)* split), replace=False)
        test_rows = [x for x in temp.index if x not in train_rows]
        temp_train = temp.loc[train_rows]
        temp_test = temp.loc[test_rows]
        df_train = df_train.append(temp_train)
        df_test = df_test.append(temp_test) 
        
    for attri in attributes:
        print (attri + '_train_nunique ' + str (df_train[attri].nunique()) )
        print (attri + '_test_nunique ' + str(df_test[attri].nunique()) )

    print  ('\nSaving final multilabel file' )
    print ('df_train.shape, df_test.shape' , df_train.shape, df_test.shape )
#     df_train.to_csv('/rapid_data/myntra/myntra_data/' + article_type + '/' + args.train_file_suffix + '.txt', sep=' ', index = False, header=False)
#     df_test.to_csv('/rapid_data/myntra/myntra_data/' + article_type + '/' +  args.test_file_suffix  + '.txt', sep=' ', index = False, header=False)
    
if __name__ == '__main__':
    args = parser.parse_args()
    main(args)