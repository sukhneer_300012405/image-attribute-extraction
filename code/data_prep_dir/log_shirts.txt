pre:(214590, 12)
post:(149189, 12)
data read..
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for neck

neck Unique valid classes: 6
neck Pre len: 14
neck Pre classes:['Spread Collar', 'Cutaway Collar', 'Mandarin Collar', 'Button-Down Collar', 'Slim Collar', 'Collarless', 'Hood', 'Club Collar', 'Built-Up Collar', 'Peter Pan Collar', 'Wingtip Collar', 'Collarless_Shirt', 'Issues_Collar', 'Borderline_Collar']
neck label_count pre:                  index   neck
0        Spread Collar  99001
1       Cutaway Collar  14487
2   Button-Down Collar  13517
3      Mandarin Collar  11491
4          Slim Collar   5392
5        Issues_Collar   2741
6           Collarless    952
7                 Hood    492
8          Club Collar    364
9     Collarless_Shirt    324
10     Built-Up Collar    210
11    Peter Pan Collar    137
12      Wingtip Collar     78
13   Borderline_Collar      2
neck Post len: 6
neck Post classes:['Spread Collar', 'Cutaway Collar', 'Mandarin Collar', 'Button-Down Collar', 'Slim Collar', 'Collarless']
neck label_count post:                 index   neck
0       Spread Collar  99001
1      Cutaway Collar  14487
2  Button-Down Collar  13517
3     Mandarin Collar  11491
4         Slim Collar   5392
5          Collarless    952
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for sleeve_length

sleeve_length Unique valid classes: 4
sleeve_length Pre len: 7
sleeve_length Pre classes:['Long Sleeves', 'Short Sleeves', 'Three-Quarter Sleeves', 'Sleeveless', 'Issues_Sleeve Length', 'Low Confidence Score Or Image Error', 'No Sleeves']
sleeve_length label_count pre:                                  index  sleeve_length
0                         Long Sleeves          95892
1                        Short Sleeves          19078
2                Three-Quarter Sleeves           3107
3                           Sleeveless           1469
4                 Issues_Sleeve Length            145
5                           No Sleeves              5
6  Low Confidence Score Or Image Error              3
sleeve_length Post len: 4
sleeve_length Post classes:['Long Sleeves', 'Short Sleeves', 'Three-Quarter Sleeves', 'Sleeveless']
sleeve_length label_count post:                    index  sleeve_length
0           Long Sleeves          95892
1          Short Sleeves          19078
2  Three-Quarter Sleeves           3107
3             Sleeveless           1469
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for length

length Unique valid classes: 3
length Pre len: 4
length Pre classes:['Regular', 'Longline', 'Crop', 'Issues_Length']
length label_count pre:            index  length
0        Regular   96472
1       Longline    3273
2           Crop     592
3  Issues_Length      66
length Post len: 3
length Post classes:['Regular', 'Longline', 'Crop']
length label_count post:       index  length
0   Regular   96472
1  Longline    3273
2      Crop     592
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for hemline

hemline Unique valid classes: 3
hemline Pre len: 7
hemline Pre classes:['Curved', 'Straight', 'High-Low', 'Asymmetric', 'Borderline_Hemline', 'Issues_Hemline', 'Image Issue In Hemline']
hemline label_count pre:                     index  hemline
0                  Curved    84333
1                Straight    14253
2                High-Low     1612
3              Asymmetric      103
4          Issues_Hemline       26
5  Image Issue In Hemline        7
6      Borderline_Hemline        2
hemline Post len: 3
hemline Post classes:['Curved', 'Straight', 'High-Low']
hemline label_count post:       index  hemline
0    Curved    84333
1  Straight    14253
2  High-Low     1612
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for print_pattern_type

print_pattern_type Unique valid classes: 24
print_pattern_type Pre len: 34
print_pattern_type Pre classes:['Solid', 'Gingham Checks', 'Micro Ditsy', 'Conversational', 'Vertical Stripes', 'Tartan Checks', 'Multi Stripes', 'Floral', 'Textured', 'Polka Dots', 'Faded', 'Micro Checks', 'Grid Tattersall Checks', 'Windowpane Checks', 'Abstract', 'Bengal Stripes', 'Other Checks', 'Geometric', 'Pinstripes', 'Buffalo Checks', 'Colourblocked', 'Horizontal Stripes', 'Ethnic Motifs', 'Graphic', 'Typography', 'Camouflage', 'Animal', 'Shepherd Checks', 'Embellished', 'Image Issue In Pattern Type', 'Striped', 'Self Design', 'Issues_Print Or Pattern Type', 'Tropical']
print_pattern_type label_count pre:                            index  print_pattern_type
0                          Solid               39096
1                   Other Checks                9138
2                  Tartan Checks                7151
3                         Floral                5053
4                      Geometric                4298
5                    Micro Ditsy                4210
6                       Textured                3936
7               Vertical Stripes                3584
8                   Micro Checks                2707
9                       Abstract                2521
10                Conversational                2520
11                Gingham Checks                2402
12                         Faded                1710
13        Grid Tattersall Checks                1572
14                    Pinstripes                1546
15            Horizontal Stripes                1239
16                Buffalo Checks                1212
17                 Ethnic Motifs                1063
18                    Polka Dots                 864
19             Windowpane Checks                 836
20                Bengal Stripes                 818
21                 Colourblocked                 759
22                 Multi Stripes                 532
23                       Graphic                 526
24                    Typography                 239
25               Shepherd Checks                 225
26                    Camouflage                 213
27                        Animal                 202
28  Issues_Print Or Pattern Type                   8
29                   Embellished                   5
30                   Self Design                   4
31                      Tropical                   2
32                       Striped                   2
33   Image Issue In Pattern Type                   2
print_pattern_type Post len: 24
print_pattern_type Post classes:['Solid', 'Gingham Checks', 'Micro Ditsy', 'Conversational', 'Vertical Stripes', 'Tartan Checks', 'Multi Stripes', 'Floral', 'Textured', 'Polka Dots', 'Faded', 'Micro Checks', 'Grid Tattersall Checks', 'Windowpane Checks', 'Abstract', 'Bengal Stripes', 'Other Checks', 'Geometric', 'Pinstripes', 'Buffalo Checks', 'Colourblocked', 'Horizontal Stripes', 'Ethnic Motifs', 'Graphic']
print_pattern_type label_count post:                      index  print_pattern_type
0                    Solid               39096
1             Other Checks                9138
2            Tartan Checks                7151
3                   Floral                5053
4                Geometric                4298
5              Micro Ditsy                4210
6                 Textured                3936
7         Vertical Stripes                3584
8             Micro Checks                2707
9                 Abstract                2521
10          Conversational                2520
11          Gingham Checks                2402
12                   Faded                1710
13  Grid Tattersall Checks                1572
14              Pinstripes                1546
15      Horizontal Stripes                1239
16          Buffalo Checks                1212
17           Ethnic Motifs                1063
18              Polka Dots                 864
19       Windowpane Checks                 836
20          Bengal Stripes                 818
21           Colourblocked                 759
22           Multi Stripes                 532
23                 Graphic                 526
len(data), len(train_rows), len(test_rows)  99293 79434 19859
df_train.shape, df_test.shape (99293, 12) (79434, 6) (19859, 6)
neck 6 {'Cutaway Collar': 0, 'Spread Collar': 1, 'Slim Collar': 2, 'Button-Down Collar': 3, 'Mandarin Collar': 4, 'Collarless': 5}
[1 4 0 3 2 5]
sleeve_length 4 {'Long Sleeves': 0, 'Short Sleeves': 1, 'Three-Quarter Sleeves': 2, 'Sleeveless': 3}
[0 2 1 3]
length 3 {'Regular': 0, 'Longline': 1, 'Crop': 2}
[0 1 2]
hemline 3 {'Curved': 0, 'Straight': 1, 'High-Low': 2}
[0 1 2]
print_pattern_type 24 {'Tartan Checks': 0, 'Micro Ditsy': 1, 'Conversational': 2, 'Geometric': 3, 'Solid': 4, 'Micro Checks': 5, 'Other Checks': 6, 'Vertical Stripes': 7, 'Textured': 8, 'Gingham Checks': 9, 'Buffalo Checks': 10, 'Faded': 11, 'Abstract': 12, 'Floral': 13, 'Grid Tattersall Checks': 14, 'Multi Stripes': 15, 'Windowpane Checks': 16, 'Pinstripes': 17, 'Polka Dots': 18, 'Bengal Stripes': 19, 'Horizontal Stripes': 20, 'Ethnic Motifs': 21, 'Graphic': 22, 'Colourblocked': 23}
[ 1 15 13 11  5  0  6  4 20  2  3 14  8 22 23  7 17 10 16 21 12 19  9 18]

Saving final multilabel file
