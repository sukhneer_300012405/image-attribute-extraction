import os
import warnings
warnings.filterwarnings('ignore')
import glob
import pandas as pd
import numpy as np
import argparse
import json
import random

article_type = 'shirts'
parser = argparse.ArgumentParser()

parser.add_argument('--myntra_file_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type +  '_valid_styles.csv')
parser.add_argument('--poshaq_file_path', default = '/rapid_data/myntra/poshaq_data/' + article_type + '/' + article_type +  '_valid_styles.csv')
parser.add_argument('--image_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type+   '_images')
parser.add_argument('--attribute_map_file', default= '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type + '_' + 'attribute_map.txt' ,type=str)
parser.add_argument('--attributes', default =['sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline'], type = str)
parser.add_argument('--output_path', default= '/rapid_data/myntra/myntra_data/' + article_type  ,type=str)
# parser.add_argument('--label_map_file_name', default = None, type = str)
parser.add_argument('--overall_map_file_name', default = article_type +  "_multilabel_overall_map_file.csv" ,type = str)
parser.add_argument('--split_ratio', default = 0.8 , type = float)
parser.add_argument('--ignore_threshold', default = 10 , type = int)
parser.add_argument('--ignore_overall_df_threshold', default = 1000000 ,type = int)

def main(args):
    data1 = pd.read_csv(args.myntra_file_path)
    data1.columns = ['style_id',  'brand', 'article_type','gender', 'color', 'image_url', \
    'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline']

#     data2 = pd.read_csv(args.poshaq_file_path)
#     data2.columns = ['style_id_dummy','origin','brand','article_type','gender','product_url','image_url','image1','image2','image3','image4','source','style_id','neck','sleeve_length','length','hemline','print_pattern_type','color','rank']
#     data2 = data2[['style_id',  'brand', 'article_type','gender', 'color', 'image_url', \
#     'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline']]
    
#     ## append poshaq and myntra data
#     data = data1.append(data2)
    data = data1.copy()
    print ('pre:' + str(data.shape) )
    data = data.dropna()
    np.random.seed(0)
#     keep_rows = np.random.choice(data.index, 51000, replace=False)
#     data = data.loc[keep_rows]
    print ('post:' + str(data.shape) )
    
    print("data read..")
    if os.path.exists(args.attribute_map_file):
        mappers = {}
        with open(args.attribute_map_file) as json_data:
            print("reading map file...")
            mappers = json.load(json_data)
            data = data.replace(mappers)
            
    data["local_image_path"] = args.image_path + "/" + data["style_id"].map(str) + ".jpg"
    data.to_csv(os.path.join(args.output_path, args.overall_map_file_name))
    print("overall csv map file created")

    attributes = args.attributes
    ignore_threshold = args.ignore_threshold
    
    for attribute in attributes:
        print ('Filtering the classes having count less than threshold value...')
        label_count = pd.DataFrame(data[attribute].value_counts()).reset_index()
        unique_val = data[attribute].unique().tolist()  
        print (attribute, 'label_count pre:', label_count)
        print  ('\nUnique ' + attribute + ' values before removing classes with very less count:' +  str(unique_val) )
        
        label_count = label_count[label_count[attribute] > ignore_threshold]       
        data = data[data[attribute].isin(label_count['index'])]
        unique_val = data[attribute].unique().tolist()
        print (attribute, 'label_count post:', label_count)
        print  ('\nUnique ' + attribute + ' Values:' +  str(unique_val) )
        
    # creating train and test rows indices
    split = args.split_ratio
    np.random.seed(0)
    train_rows = np.random.choice(data.index, int(len(data)* split), replace=False)
    test_rows = [x for x in data.index if x not in train_rows]
        
        
    for attribute in attributes:   
        directory = os.path.join(args.output_path, article_type + '_' + attribute)
        if not os.path.exists(directory):
            print("creating directory for attribute :\n"+ attribute)
            os.makedirs(directory)

        overall_df = data[['style_id', 'image_url', 'local_image_path', attribute]]
        label_count = pd.DataFrame(overall_df[attribute].value_counts()).reset_index()
        unique_val = overall_df[attribute].unique().tolist()
        print  ('\nUnique ' + attribute + ' Values:' +  str(unique_val) )
        
        
        labels_dict = {i:j for j,i in enumerate(unique_val)}
#         print ('\n' + str(labels_dict))
#         print ('\nWriting label map file')
        with open(directory + '/' + article_type + '_' + attribute + '_label_map.txt', 'w') as file:
            file.write(json.dumps(labels_dict))
            
        df_train = overall_df.loc[train_rows]
        df_test = overall_df.loc[test_rows]
        df_train[attribute] = df_train[attribute].map(labels_dict)
        df_test[attribute] = df_test[attribute].map(labels_dict)
        df_train = df_train[['local_image_path', attribute]]
        df_test = df_test[['local_image_path', attribute]]
        
        print (attribute + '_train_nunique ' + str (df_train[attribute].unique()) )
        print (attribute + '_test_nunique ' + str(df_test[attribute].unique()))

        print  ('\nSaving final multilabel file' )
        df_train.to_csv(directory + '/' + article_type + '_' + attribute+ '_'  + 'new_train.txt', sep=' ', index = False, header=False)
        df_test.to_csv(directory + '/' + article_type + '_' + attribute+ '_' + 'new_test.txt', sep=' ', index = False, header=False)
        
    ############# Multi-label file preparation
    
    cols_to_keep = ['local_image_path'] + attributes
    multilabel_df = data[cols_to_keep]
    df_train = multilabel_df.loc[train_rows]
    df_test = multilabel_df.loc[test_rows]
    
#     print ('multilabel_df data set\n')
#     for col in [x for x in multilabel_df.columns if x != 'local_image_path']:
#         print (col + ':' +  str (multilabel_df[col].value_counts(normalize = False) ) )

#     print ('Train data set\n')
#     for col in [x for x in df_train.columns if x != 'local_image_path']:
#         print (col + ': ' +  str (df_train[col].nunique() ) )
#         print (col + ':' +  str (df_train[col].value_counts(normalize = False) ) )
        
#     print ('Test data set\n')
#     for col in [x for x in df_test.columns if x != 'local_image_path']:
#         print (col + ': ' +  str (df_test[col].nunique() ))
#         print (col + ':' +  str (df_test[col].value_counts(normalize = False) ) )
        
    for attribute in attributes:
        label_map_file = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type + '_' + attribute + '/' + \
        article_type + '_' + attribute +  '_label_map.txt'
        with open(label_map_file) as json_data:
            labels_dict = json.load(json_data) 
#         print (attribute, labels_dict)
        df_train[attribute] = df_train[attribute].map(labels_dict)
        df_test[attribute] = df_test[attribute].map(labels_dict)

    
    df_train.to_csv('/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/multi_label_train.txt', sep=' ', index = False, header=False)
    df_test.to_csv('/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/multi_label_test.txt', sep=' ', index = False, header=False)
        
    ############# Multi-label OHE file preparation  
        
    multilabel_OHE_df = pd.get_dummies(multilabel_df, columns = attributes)
    multilabel_OHE_df.to_csv('/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/OHE_label.txt', sep= ' ', index = False)
    df_train = multilabel_OHE_df.loc[train_rows]
    df_test = multilabel_OHE_df.loc[test_rows]
    


    print  ('\nSaving final multilabel file' )
    df_train.to_csv('/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/OHE_multi_label_train.txt', sep=' ', index = False, header=False)
    df_test.to_csv('/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/OHE_multi_label_test.txt', sep=' ', index = False, header=False)
 

if __name__ == '__main__':
    args = parser.parse_args()
    main(args)