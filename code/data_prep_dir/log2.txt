Index(['495562002', 'hnmdeoffline', 'H&M', 'Shirts', 'Men',
       'https://www.hm.com/de/products/search?q=0495562002',
       'http://lp.hm.com/hmprod?set=source%5B%2Fmodel%2F2017%2FF00%200495562%20002%2055%200809.jpg%5D%2Cmedia_type%5BSTILL_LIFE_FRONT%5D%2Ctshirt_size%5BS%5D%2Cquality%5BL%5D%2Chmver%5B1%5D&call=url%5Bfile%3A%2Flegacy%2Fv1%2Fproduct.chain%5D',
       'http://lp.hm.com/hmprod?set=source%5B%2Fmodel%2F2017%2FF00%200495562%20002%2055%200809.jpg%5D%2Cmedia_type%5BSTILL_LIFE_FRONT%5D%2Ctshirt_size%5BS%5D%2Cquality%5BL%5D%2Chmver%5B1%5D&call=url%5Bfile%3A%2Flegacy%2Fv1%2Fproduct.chain%5D.1',
       'Unnamed: 8', 'Unnamed: 9', 'Unnamed: 10', 'hnmdeoffline.1',
       '495562002.1', 'Spread Collar', 'Long Sleeves', 'Regular', 'Curved',
       'Solid', 'black', '1'],
      dtype='object') 20
pre:(42506, 11)
post:(40857, 11)
data read..
overall csv map file created
Filtering the classes having count less than threshold value...
sleeve_length label_count pre:                                  index  sleeve_length
0                         Long Sleeves          28665
1                        Short Sleeves           7502
2                 Issues_Sleeve Length           2684
3                Three-Quarter Sleeves           1621
4                           Sleeveless            375
5                           No Sleeves              5
6  Low confidence score or image error              3
7                          Image Issue              2

Unique sleeve_length values before removing classes with very less count:['Short Sleeves', 'Three-Quarter Sleeves', 'Long Sleeves', 'Issues_Sleeve Length', 'Sleeveless', 'Image Issue', 'Low confidence score or image error', 'No Sleeves']
sleeve_length label_count post:                    index  sleeve_length
0           Long Sleeves          28665
1          Short Sleeves           7502
2   Issues_Sleeve Length           2684
3  Three-Quarter Sleeves           1621
4             Sleeveless            375

Unique sleeve_length Values:['Short Sleeves', 'Three-Quarter Sleeves', 'Long Sleeves', 'Issues_Sleeve Length', 'Sleeveless']
Filtering the classes having count less than threshold value...
length label_count pre:                                  index  length
0                              Regular   34382
1                             Longline    3829
2                        Issues_Length    2296
3                                 Crop     260
4                          Image_Issue      77
5                                 LONG       1
6  Low confidence score or image error       1
7        Image is not clearly visible.       1

Unique length values before removing classes with very less count:['Regular', 'Longline', 'Crop', 'Issues_Length', 'Image_Issue', 'Low confidence score or image error', 'LONG', 'Image is not clearly visible.']
length label_count post:            index  length
0        Regular   34382
1       Longline    3829
2  Issues_Length    2296
3           Crop     260

Unique length Values:['Regular', 'Longline', 'Crop', 'Issues_Length']
Filtering the classes having count less than threshold value...
print_pattern_type label_count pre:                                   index  print_pattern_type
0                                 Solid               18673
1                                Floral                3066
2                      Vertical Stripes                2470
3                          Other Checks                2243
4          Issues_Print or Pattern Type                2156
5                              Textured                1538
6                        Conversational                1528
7                           Micro Ditsy                 959
8                          Micro Checks                 878
9                         Tartan Checks                 877
10                             Abstract                 829
11                           Pinstripes                 739
12                                Faded                 645
13                           Polka Dots                 635
14                       Buffalo Checks                 565
15                       Gingham Checks                 482
16                   Horizontal Stripes                 334
17                            Geometric                 324
18                        Colourblocked                 302
19                        Ethnic Motifs                 274
20                    Windowpane Checks                 224
21                       Bengal Stripes                 218
22                               Animal                 180
23                           Camouflage                 142
24                           Typography                 141
25               Grid Tattersall Checks                 124
26                              Graphic                  94
27                        Multi Stripes                  53
28                      Shepherd Checks                  23
29                          Embellished                  14
30                          Self Design                   8
31                                SOLID                   6
32                       Bengal stripes                   5
33                             Tropical                   3
34          image issue in pattern Type                   2
35  Low confidence score or image error                   2
36                              Striped                   2
37                               ANIMAL                   1
38                       conversational                   1
39                                FADED                   1
40                          self design                   1
41                         Alphanumeric                   1
42                          Self design                   1
43                                  YES                   1
44                        Buffalo Check                   1
45                              GRAPHIC                   1

Unique print_pattern_type values before removing classes with very less count:['Bengal Stripes', 'Vertical Stripes', 'Pinstripes', 'Camouflage', 'Conversational', 'Solid', 'Micro Checks', 'Floral', 'Other Checks', 'Buffalo Checks', 'Faded', 'Textured', 'Polka Dots', 'Tartan Checks', 'Ethnic Motifs', 'Gingham Checks', 'Issues_Print or Pattern Type', 'Geometric', 'Abstract', 'Colourblocked', 'Micro Ditsy', 'Horizontal Stripes', 'Animal', 'Windowpane Checks', 'Graphic', 'Typography', 'Bengal stripes', 'Shepherd Checks', 'Grid Tattersall Checks', 'Multi Stripes', 'Embellished', 'image issue in pattern Type', 'Striped', 'conversational', 'SOLID', 'Alphanumeric', 'Self Design', 'Self design', 'Buffalo Check', 'ANIMAL', 'Low confidence score or image error', 'Tropical', 'FADED', 'YES', 'GRAPHIC', 'self design']
print_pattern_type label_count post:                            index  print_pattern_type
0                          Solid               18673
1                         Floral                3066
2               Vertical Stripes                2470
3                   Other Checks                2243
4   Issues_Print or Pattern Type                2156
5                       Textured                1538
6                 Conversational                1528
7                    Micro Ditsy                 959
8                   Micro Checks                 878
9                  Tartan Checks                 877
10                      Abstract                 829
11                    Pinstripes                 739
12                         Faded                 645
13                    Polka Dots                 635
14                Buffalo Checks                 565
15                Gingham Checks                 482
16            Horizontal Stripes                 334
17                     Geometric                 324
18                 Colourblocked                 302
19                 Ethnic Motifs                 274
20             Windowpane Checks                 224
21                Bengal Stripes                 218

Unique print_pattern_type Values:['Bengal Stripes', 'Vertical Stripes', 'Pinstripes', 'Conversational', 'Solid', 'Micro Checks', 'Floral', 'Other Checks', 'Buffalo Checks', 'Faded', 'Textured', 'Polka Dots', 'Tartan Checks', 'Ethnic Motifs', 'Gingham Checks', 'Issues_Print or Pattern Type', 'Geometric', 'Abstract', 'Colourblocked', 'Micro Ditsy', 'Horizontal Stripes', 'Windowpane Checks']
Filtering the classes having count less than threshold value...
neck label_count pre:                                index   neck
0                      Spread Collar  22328
1                 Button-Down Collar   5994
2                    Mandarin Collar   3135
3                        Slim Collar   3042
4                      Issues_Collar   2642
5                     Cutaway Collar   1523
6                         Collarless    430
7                   Collarless_Shirt    320
8                        Club Collar    229
9                    Built-Up Collar    138
10                  Peter Pan Collar     66
11                              Hood     60
12                    Wingtip Collar     46
13                       Slim collar      2
14                                        1
15                        collarless      1
16                       Club collar      1
17                 Borderline_Collar      1

Unique neck values before removing classes with very less count:['Collarless_Shirt', 'Mandarin Collar', 'Spread Collar', 'Cutaway Collar', 'Slim Collar', 'Button-Down Collar', 'Hood', 'Club Collar', 'Issues_Collar', 'Peter Pan Collar', 'Collarless', 'Club collar', 'Built-Up Collar', 'Wingtip Collar', 'Slim collar', 'Borderline_Collar', 'collarless', '                                ']
neck label_count post:                 index   neck
0       Spread Collar  22328
1  Button-Down Collar   5994
2     Mandarin Collar   3135
3         Slim Collar   3042
4       Issues_Collar   2642
5      Cutaway Collar   1523
6          Collarless    430
7    Collarless_Shirt    320
8         Club Collar    229

Unique neck Values:['Collarless_Shirt', 'Mandarin Collar', 'Spread Collar', 'Cutaway Collar', 'Slim Collar', 'Button-Down Collar', 'Club Collar', 'Issues_Collar', 'Collarless']
Filtering the classes having count less than threshold value...
hemline label_count pre:                            index  hemline
0                         Curved    26869
1                       Straight     8641
2                 Issues_Hemline     2748
3                       High-Low     1275
4                     Asymmetric       92
5         image issue in hemline       13
6             Borderline_Hemline        3
7                            YES        1
8  Image is not clearly visible.        1

Unique hemline values before removing classes with very less count:['High-Low', 'Curved', 'Straight', 'Issues_Hemline', 'Asymmetric', 'Borderline_Hemline', 'image issue in hemline', 'YES', 'Image is not clearly visible.']
hemline label_count post:             index  hemline
0          Curved    26869
1        Straight     8641
2  Issues_Hemline     2748
3        High-Low     1275

Unique hemline Values:['High-Low', 'Curved', 'Straight', 'Issues_Hemline']

Unique sleeve_length Values:['Short Sleeves', 'Three-Quarter Sleeves', 'Long Sleeves', 'Issues_Sleeve Length', 'Sleeveless']
sleeve_length_train_nunique [0 3 2 1 4]
sleeve_length_test_nunique [2 0 1 3 4]

Saving final multilabel file

Unique length Values:['Regular', 'Longline', 'Crop', 'Issues_Length']
length_train_nunique [0 3 1 2]
length_test_nunique [0 3 1 2]

Saving final multilabel file

Unique print_pattern_type Values:['Bengal Stripes', 'Vertical Stripes', 'Pinstripes', 'Conversational', 'Solid', 'Micro Checks', 'Floral', 'Other Checks', 'Buffalo Checks', 'Faded', 'Textured', 'Polka Dots', 'Tartan Checks', 'Gingham Checks', 'Issues_Print or Pattern Type', 'Geometric', 'Abstract', 'Colourblocked', 'Micro Ditsy', 'Horizontal Stripes', 'Windowpane Checks', 'Ethnic Motifs']
print_pattern_type_train_nunique [ 3 14  8  4  5  6  1  7 20  9  2 12 19 21 18 15 10 16 13 11  0 17]
print_pattern_type_test_nunique [ 6  7  8  4 11  1  9 14 10 17 12 13 19  2  3 18 16 15 21  5 20  0]

Saving final multilabel file

Unique neck Values:['Collarless_Shirt', 'Mandarin Collar', 'Spread Collar', 'Cutaway Collar', 'Slim Collar', 'Button-Down Collar', 'Club Collar', 'Issues_Collar', 'Collarless']
neck_train_nunique [5 7 1 4 2 3 6 8 0]
neck_test_nunique [3 2 5 4 7 1 8 0 6]

Saving final multilabel file

Unique hemline Values:['High-Low', 'Curved', 'Straight', 'Issues_Hemline']
hemline_train_nunique [1 3 2 0]
hemline_test_nunique [2 1 3 0]

Saving final multilabel file
multilabel_df data set

sleeve_length:Long Sleeves             27738
Short Sleeves             7303
Issues_Sleeve Length      2584
Three-Quarter Sleeves     1553
Sleeveless                 355
Name: sleeve_length, dtype: int64
length:Regular          33378
Longline          3642
Issues_Length     2269
Crop               244
Name: length, dtype: int64
print_pattern_type:Solid                           18439
Floral                           3041
Vertical Stripes                 2434
Other Checks                     2222
Issues_Print or Pattern Type     2154
Textured                         1525
Conversational                   1514
Micro Ditsy                       958
Micro Checks                      871
Tartan Checks                     861
Abstract                          811
Pinstripes                        735
Faded                             642
Polka Dots                        629
Buffalo Checks                    561
Gingham Checks                    477
Horizontal Stripes                331
Geometric                         322
Colourblocked                     297
Ethnic Motifs                     273
Windowpane Checks                 219
Bengal Stripes                    217
Name: print_pattern_type, dtype: int64
neck:Spread Collar         22252
Button-Down Collar     5994
Mandarin Collar        3125
Slim Collar            3033
Issues_Collar          2641
Cutaway Collar         1518
Collarless              425
Collarless_Shirt        317
Club Collar             228
Name: neck, dtype: int64
hemline:Curved            26869
Straight           8641
Issues_Hemline     2748
High-Low           1275
Name: hemline, dtype: int64
Train data set

sleeve_length: 5
sleeve_length:Long Sleeves             22200
Short Sleeves             5831
Issues_Sleeve Length      2069
Three-Quarter Sleeves     1260
Sleeveless                 266
Name: sleeve_length, dtype: int64
length: 4
length:Regular          26694
Longline          2913
Issues_Length     1825
Crop               194
Name: length, dtype: int64
print_pattern_type: 22
print_pattern_type:Solid                           14752
Floral                           2440
Vertical Stripes                 1929
Other Checks                     1783
Issues_Print or Pattern Type     1742
Textured                         1216
Conversational                   1200
Micro Ditsy                       754
Tartan Checks                     706
Micro Checks                      702
Abstract                          647
Pinstripes                        586
Faded                             517
Polka Dots                        508
Buffalo Checks                    443
Gingham Checks                    379
Horizontal Stripes                269
Geometric                         259
Colourblocked                     236
Ethnic Motifs                     219
Bengal Stripes                    172
Windowpane Checks                 167
Name: print_pattern_type, dtype: int64
neck: 9
neck:Spread Collar         17765
Button-Down Collar     4812
Mandarin Collar        2519
Slim Collar            2433
Issues_Collar          2131
Cutaway Collar         1221
Collarless              328
Collarless_Shirt        239
Club Collar             178
Name: neck, dtype: int64
hemline: 4
hemline:Curved            21497
Straight           6899
Issues_Hemline     2216
High-Low           1014
Name: hemline, dtype: int64
Test data set

sleeve_length: 5
sleeve_length:Long Sleeves             5538
Short Sleeves            1472
Issues_Sleeve Length      515
Three-Quarter Sleeves     293
Sleeveless                 89
Name: sleeve_length, dtype: int64
length: 4
length:Regular          6684
Longline          729
Issues_Length     444
Crop               50
Name: length, dtype: int64
print_pattern_type: 22
print_pattern_type:Solid                           3687
Floral                           601
Vertical Stripes                 505
Other Checks                     439
Issues_Print or Pattern Type     412
Conversational                   314
Textured                         309
Micro Ditsy                      204
Micro Checks                     169
Abstract                         164
Tartan Checks                    155
Pinstripes                       149
Faded                            125
Polka Dots                       121
Buffalo Checks                   118
Gingham Checks                    98
Geometric                         63
Horizontal Stripes                62
Colourblocked                     61
Ethnic Motifs                     54
Windowpane Checks                 52
Bengal Stripes                    45
Name: print_pattern_type, dtype: int64
neck: 9
neck:Spread Collar         4487
Button-Down Collar    1182
Mandarin Collar        606
Slim Collar            600
Issues_Collar          510
Cutaway Collar         297
Collarless              97
Collarless_Shirt        78
Club Collar             50
Name: neck, dtype: int64
hemline: 4
hemline:Curved            5372
Straight          1742
Issues_Hemline     532
High-Low           261
Name: hemline, dtype: int64
sleeve_length {'Long Sleeves': 0, 'Short Sleeves': 1, 'Three-Quarter Sleeves': 2, 'Sleeveless': 3}
length {'Regular': 0, 'Longline': 1, 'Crop': 2}
print_pattern_type {'Tartan Checks': 0, 'Micro Ditsy': 1, 'Conversational': 2, 'Geometric': 3, 'Solid': 4, 'Micro Checks': 5, 'Other Checks': 6, 'Vertical Stripes': 7, 'Textured': 8, 'Gingham Checks': 9, 'Buffalo Checks': 10, 'Faded': 11, 'Abstract': 12, 'Floral': 13, 'Grid Tattersall Checks': 14, 'Multi Stripes': 15, 'Windowpane Checks': 16, 'Pinstripes': 17, 'Polka Dots': 18, 'Bengal Stripes': 19, 'Horizontal Stripes': 20, 'Ethnic Motifs': 21, 'Graphic': 22, 'Colourblocked': 23}
neck {'Cutaway Collar': 0, 'Spread Collar': 1, 'Slim Collar': 2, 'Button-Down Collar': 3, 'Mandarin Collar': 4, 'Collarless': 5}
hemline {'Curved': 0, 'Straight': 1, 'High-Low': 2}

Saving final multilabel file
