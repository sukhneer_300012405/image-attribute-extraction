import os
import warnings
warnings.filterwarnings('ignore')
import glob
import pandas as pd
import numpy as np
import argparse
import json
import random

article_type = 'tops'
parser = argparse.ArgumentParser()

parser.add_argument('--myntra_file_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type +  '_valid_styles.csv')
parser.add_argument('--poshaq_file_path', default = '/rapid_data/myntra/poshaq_data/' + article_type + '/' + article_type +  '_valid_styles.csv')
parser.add_argument('--mynt_image_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type+   '_images')
parser.add_argument('--posh_image_path', default = '/rapid_data/myntra/poshaq_data/' + article_type + '/' + article_type+   '_images')
parser.add_argument('--attribute_map_file', default= '/rapid_data/myntra/myntra_data/' + article_type + '/' + article_type + '_' + 'attribute_map.txt' ,type=str)
parser.add_argument('--label_map_file', default= /rapid_data/myntra/myntra_data/' + article_type + '/' + article_type + '_' + attribute + '/' + article_type + '_' + attribute +  '_label_map_with_len.txt' ,type=str)
#parser.add_argument('--attributes',default =['sleeve_style','sleeve_length','print_pattern_type','neck'], type = str)
parser.add_argument('--attributes',default =['length'], type = str)

parser.add_argument('--output_path', default= '/rapid_data/myntra/myntra_data/' + article_type  ,type=str)
# parser.add_argument('--label_map_file_name', default = None, type = str)
parser.add_argument('--overall_map_file_name', default = article_type +  "_multilabel_overall_map_file.csv" ,type = str)
parser.add_argument('--split_ratio', default = 0.8 ,type = float)
parser.add_argument('--ignore_threshold', default = 1 ,type = int)
parser.add_argument('--keep_threshold', default = 100000 ,type = int)

def main(args):
    
    data1 = pd.read_csv(args.myntra_file_path)    

#     data1.columns = ['style_id',  'brand', 'article_type','gender', 'color', 'image_url', \
#     'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline']
    
    data1.columns = ['style_id',  'brand', 'article_type','gender', 'color', 'image_url', \
                    'sleeve_style', 'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline', 'shape']
    #data1.drop(['hemline', 'shape', 'length'], axis = 1 , inplace = True)
        
    
    data1["local_image_path"] = args.mynt_image_path + "/" + data1["style_id"].map(str) + ".jpg"

    data2 = pd.read_csv(args.poshaq_file_path)
    
    data2.columns = ['style_id','origin','brand','article_type','gender','product_url','image_url','image1', \
                     'image2','image3','image4','source','style_id2','neck','sleeve_length','type', \
                     'print_pattern_type', 'sleeve_style','length' , 'color','rank']
    
#     style_id        origin  brand   article_type    gender  product_url     default_image   image1  image2  image3  image4  source  style_id        neck    sleeve_length   type    print_or_pattern_type   sleeve_styling  length  colour  rank
       
    data2 =  data2[['style_id',  'brand', 'article_type', 'gender', 'color', 'image_url', 'sleeve_style',
    'sleeve_length','length', 'print_pattern_type', 'neck']]
    
    
    data2[args.attributes] = data2[args.attributes].apply(lambda x : x.str.title() )
    
    data2["local_image_path"] = args.posh_image_path + "/" + data2["style_id"].map(str) + ".jpg"
    
#     ## append poshaq and myntra data
    data = data1.append(data2)
#     data = data1.copy()
    print ('pre:' + str(data.shape) )
    data = data.dropna()
#     np.random.seed(0)
#     keep_rows = np.random.choice(data.index, 51000, replace=False)
#     data = data.loc[keep_rows]
    print ('post:' + str(data.shape) )
    
    print("data read..")
    if os.path.exists(args.attribute_map_file):
        mappers = {}
        with open(args.attribute_map_file) as json_data:
            print("reading map file...")
            mappers = json.load(json_data)
            data = data.replace(mappers)

#     data.to_csv(os.path.join(args.output_path, args.overall_map_file_name))
#     print("overall csv map file created")

    attributes = args.attributes
    ignore_threshold = args.ignore_threshold
    keep_threshold = args.keep_threshold
    
    for attribute in attributes:
        print ('Filtering the classes having count less than threshold value...')
        label_count = pd.DataFrame(data[attribute].value_counts()).reset_index()
        unique_val = data[attribute].unique().tolist()  
#         print (attribute, 'label_count pre:', label_count)
#         print  ('\nUnique ' + attribute + ' values before removing rare classes:' +  str(unique_val) )
        
        label_count = label_count[label_count[attribute] > ignore_threshold]       
        data = data[data[attribute].isin(label_count['index'])]
        unique_val = data[attribute].unique().tolist()
#         print (attribute, 'label_count post:', label_count)
#         print  ('\nUnique ' + attribute + ' values after removing rare classes:' +  str(unique_val) )
        
        unique_classes = data[attribute].unique().tolist()
        
        print ('Capping the classes having count greater than threshold value...')
        for classes in unique_classes:
            random.seed(1)
            total_class_rows = sum(data[attribute] == classes)
            if total_class_rows > keep_threshold:
                extra_rows = total_class_rows - keep_threshold
#                 print (extra_rows , len(list( data[data[attribute] == classes].index) ))
                drop_rows = random.sample( list(data[data[attribute] == classes].index), extra_rows )
                data = data.drop(drop_rows)
                
        label_count = pd.DataFrame(data[attribute].value_counts()).reset_index()
#         print (attribute, 'label_count:', label_count)
#         print  ('\nUnique ' + attribute + ' values after capping:' +  str(unique_val)  )
        
        
        print ('Removing invalid classes for {}\n'.format(attribute) )
        
        label_map_file = args.label_map_file
                    
        with open(label_map_file) as json_data:
            labels_dict = json.load(json_data) 
            
        unique_val = []
        for key in labels_dict.keys():
            unique_val.append(key)

        print (attribute +  ' Unique valid classes: ' + str(len(unique_val)) )
        print (attribute +  ' Pre len: ' + str(len(data[attribute].unique().tolist())) )
        print  (attribute + ' Pre classes:' + str(data[attribute].unique().tolist() ) )
        label_count = pd.DataFrame(data[attribute].value_counts()).reset_index() 
        print (attribute, 'label_count pre:', label_count)
        
        data = data[data[attribute].isin(unique_val)]
        print (attribute +  ' Post len: ' + str(len(data[attribute].unique().tolist())) )
        print  (attribute + ' Post classes:' + str(data[attribute].unique().tolist() ) )
        
        label_count = pd.DataFrame(data[attribute].value_counts()).reset_index() 
        print (attribute, 'label_count post:', label_count)

        
    # creating train and test rows indices
    split = args.split_ratio
    np.random.seed(0)
    data.reset_index(drop = True, inplace = True)
    train_rows = np.random.choice(data.index, int(len(data)* split), replace=False)
    test_rows = [x for x in data.index if x not in train_rows]
    print ('len(data), len(train_rows), len(test_rows) ', len(data), len(train_rows), len(test_rows) )
        
        
    for attribute in attributes:   
        directory = os.path.join(args.output_path, article_type + '_' + attribute)
        if not os.path.exists(directory):
            print("creating directory for attribute :\n"+ attribute)
            os.makedirs(directory)

        overall_df = data[['style_id', 'image_url', 'local_image_path', attribute]]

        label_map_file = args.label_map_file
        
        with open(label_map_file) as json_data:
            labels_dict = json.load(json_data) 
        
            
        df_train = overall_df.loc[train_rows]
        df_test = overall_df.loc[test_rows]
        df_train[attribute] = df_train[attribute].map(labels_dict)
        df_test[attribute] = df_test[attribute].map(labels_dict)
        df_train = df_train[['local_image_path', attribute]]
        df_test = df_test[['local_image_path', attribute]]
        
        print (directory + '/' + article_type + '_' + attribute + '_'  + 'mynt_posh_train.txt')
        df_train.to_csv(directory + '/' + article_type + '_' + attribute + '_'  + 'mynt_posh_train.txt', sep=' ', index = False, header=False)
        df_test.to_csv(directory + '/' + article_type + '_' + attribute + '_' + 'mynt_posh_test.txt', sep=' ', index = False, header=False)
        
    ############# Multi-label file preparation
    
    cols_to_keep = ['local_image_path'] + attributes
    multilabel_df = data[cols_to_keep]
    df_train = multilabel_df.loc[train_rows]
    df_test = multilabel_df.loc[test_rows]
    print ('df_train.shape, df_test.shape', data.shape, df_train.shape, df_test.shape)
    
#     print ('multilabel_df data set\n')
#     for col in [x for x in multilabel_df.columns if x != 'local_image_path']:
#         print (col + ':' +  str (multilabel_df[col].value_counts(normalize = False) ) )

#     print ('Train data set\n')
#     for col in [x for x in df_train.columns if x != 'local_image_path']:
#         print (col + ': ' +  str (df_train[col].nunique() ) )
#         print (col + ':' +  str (df_train[col].value_counts(normalize = True) ) )
        
#     print ('Test data set\n')
#     for col in [x for x in df_test.columns if x != 'local_image_path']:
#         print (col + ': ' +  str (df_test[col].nunique() ))
#         print (col + ':' +  str (df_test[col].value_counts(normalize = True) ) )
        
    for attribute in attributes:
        label_map_file = args.label_map_file
        with open(label_map_file) as json_data:
            labels_dict = json.load(json_data) 
        print (attribute, len(labels_dict), labels_dict )
        df_train[attribute] = df_train[attribute].map(labels_dict)
        df_test[attribute] = df_test[attribute].map(labels_dict)
        print (df_test[attribute].unique() )
        
    multi_label_dir = args.output_path + '/'+ 'multi_label'
    if not os.path.exists(directory):
        print("creating directory")  
        os.makedirs(multi_label_dir)

    df_train.to_csv(multi_label_dir + '/multi_label_mynt_posh_train.txt', sep=' ', index = False, header=False)
    df_test.to_csv(multi_label_dir + '/multi_label_mynt_posh_test.txt', sep=' ', index = False, header=False)
        
    ############# Multi-label OHE file preparation  
        
    multilabel_OHE_df = pd.get_dummies(multilabel_df, columns = attributes)
#     multilabel_OHE_df.to_csv(multi_label_dir + '/OHE_label.txt', sep= ' ', index = False)
    df_train = multilabel_OHE_df.loc[train_rows]
    df_test = multilabel_OHE_df.loc[test_rows]

    print  ('\nSaving final multilabel file' )
    df_train.to_csv(multi_label_dir + '/OHE_multi_label_mynt_posh_train.txt', sep=' ', index = False, header=False)
    df_test.to_csv(multi_label_dir + '/OHE_multi_label_mynt_posh_test.txt', sep=' ', index = False, header=False)
 

if __name__ == '__main__':
    args = parser.parse_args()
    main(args)