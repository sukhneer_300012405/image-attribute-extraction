pre:(214590, 12)
post:(149189, 12)
data read..
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for sleeve_length

sleeve_length Unique valid classes: 4
sleeve_length Pre len: 8
sleeve_length Pre classes:['Long Sleeves', 'Short Sleeves', 'Three-Quarter Sleeves', 'Sleeveless', 'Issues_Sleeve Length', 'Image Issue', 'Low Confidence Score Or Image Error', 'No Sleeves']
sleeve_length label_count pre:                                  index  sleeve_length
0                         Long Sleeves          95599
1                        Short Sleeves          19151
2                Three-Quarter Sleeves           3238
3                 Issues_Sleeve Length           2419
4                           Sleeveless           1499
5                           No Sleeves              4
6  Low Confidence Score Or Image Error              3
7                          Image Issue              2
sleeve_length Post len: 4
sleeve_length Post classes:['Long Sleeves', 'Short Sleeves', 'Three-Quarter Sleeves', 'Sleeveless']
sleeve_length label_count post:                    index  sleeve_length
0           Long Sleeves          95599
1          Short Sleeves          19151
2  Three-Quarter Sleeves           3238
3             Sleeveless           1499
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for length

length Unique valid classes: 3
length Pre len: 5
length Pre classes:['Regular', 'Longline', 'Crop', 'Issues_Length', 'Image_Issue']
length label_count pre:            index  length
0        Regular   96485
1       Longline    3381
2           Crop     600
3  Issues_Length     148
4    Image_Issue       3
length Post len: 3
length Post classes:['Regular', 'Longline', 'Crop']
length label_count post:       index  length
0   Regular   96485
1  Longline    3381
2      Crop     600
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for print_pattern_type

print_pattern_type Unique valid classes: 28
print_pattern_type Pre len: 33
print_pattern_type Pre classes:['Geometric', 'Gingham Checks', 'Micro Ditsy', 'Other Checks', 'Solid', 'Conversational', 'Vertical Stripes', 'Tartan Checks', 'Floral', 'Textured', 'Faded', 'Windowpane Checks', 'Abstract', 'Bengal Stripes', 'Pinstripes', 'Micro Checks', 'Buffalo Checks', 'Grid Tattersall Checks', 'Colourblocked', 'Horizontal Stripes', 'Ethnic Motifs', 'Graphic', 'Multi Stripes', 'Typography', 'Polka Dots', 'Animal', 'Shepherd Checks', 'Camouflage', 'Issues_Print Or Pattern Type', 'Embellished', 'Self Design', 'Low Confidence Score Or Image Error', 'Tropical']
print_pattern_type label_count pre:                                   index  print_pattern_type
0                                 Solid               39165
1                          Other Checks                9134
2                         Tartan Checks                7254
3                                Floral                5064
4                             Geometric                4255
5                           Micro Ditsy                4197
6                              Textured                3903
7                      Vertical Stripes                3625
8                          Micro Checks                2744
9                        Conversational                2533
10                             Abstract                2492
11                       Gingham Checks                2445
12                                Faded                1709
13                           Pinstripes                1570
14               Grid Tattersall Checks                1549
15                   Horizontal Stripes                1228
16                       Buffalo Checks                1193
17                        Ethnic Motifs                1071
18                           Polka Dots                 883
19                    Windowpane Checks                 858
20                       Bengal Stripes                 817
21                        Colourblocked                 758
22                              Graphic                 554
23                        Multi Stripes                 521
24                           Typography                 232
25                           Camouflage                 222
26                      Shepherd Checks                 214
27                               Animal                 195
28         Issues_Print Or Pattern Type                  57
29                          Embellished                   8
30                          Self Design                   8
31                             Tropical                   2
32  Low Confidence Score Or Image Error                   2
print_pattern_type Post len: 28
print_pattern_type Post classes:['Geometric', 'Gingham Checks', 'Micro Ditsy', 'Other Checks', 'Solid', 'Conversational', 'Vertical Stripes', 'Tartan Checks', 'Floral', 'Textured', 'Faded', 'Windowpane Checks', 'Abstract', 'Bengal Stripes', 'Pinstripes', 'Micro Checks', 'Buffalo Checks', 'Grid Tattersall Checks', 'Colourblocked', 'Horizontal Stripes', 'Ethnic Motifs', 'Graphic', 'Multi Stripes', 'Typography', 'Polka Dots', 'Animal', 'Shepherd Checks', 'Camouflage']
print_pattern_type label_count post:                      index  print_pattern_type
0                    Solid               39165
1             Other Checks                9134
2            Tartan Checks                7254
3                   Floral                5064
4                Geometric                4255
5              Micro Ditsy                4197
6                 Textured                3903
7         Vertical Stripes                3625
8             Micro Checks                2744
9           Conversational                2533
10                Abstract                2492
11          Gingham Checks                2445
12                   Faded                1709
13              Pinstripes                1570
14  Grid Tattersall Checks                1549
15      Horizontal Stripes                1228
16          Buffalo Checks                1193
17           Ethnic Motifs                1071
18              Polka Dots                 883
19       Windowpane Checks                 858
20          Bengal Stripes                 817
21           Colourblocked                 758
22                 Graphic                 554
23           Multi Stripes                 521
24              Typography                 232
25              Camouflage                 222
26         Shepherd Checks                 214
27                  Animal                 195
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for neck

neck Unique valid classes: 11
neck Pre len: 14
neck Pre classes:['Mandarin Collar', 'Spread Collar', 'Button-Down Collar', 'Cutaway Collar', 'Slim Collar', 'Hood', 'Collarless', 'Club Collar', 'Peter Pan Collar', 'Wingtip Collar', 'Built-Up Collar', 'Collarless_Shirt', 'Issues_Collar', 'Borderline_Collar']
neck label_count pre:                  index   neck
0        Spread Collar  68104
1       Cutaway Collar  10035
2   Button-Down Collar   9058
3      Mandarin Collar   7845
4          Slim Collar   3587
5           Collarless    655
6                 Hood    358
7          Club Collar    253
8     Collarless_Shirt    197
9      Built-Up Collar    132
10    Peter Pan Collar     96
11      Wingtip Collar     50
12       Issues_Collar     13
13   Borderline_Collar      2
neck Post len: 11
neck Post classes:['Mandarin Collar', 'Spread Collar', 'Button-Down Collar', 'Cutaway Collar', 'Slim Collar', 'Hood', 'Collarless', 'Club Collar', 'Peter Pan Collar', 'Wingtip Collar', 'Built-Up Collar']
neck label_count post:                  index   neck
0        Spread Collar  68104
1       Cutaway Collar  10035
2   Button-Down Collar   9058
3      Mandarin Collar   7845
4          Slim Collar   3587
5           Collarless    655
6                 Hood    358
7          Club Collar    253
8      Built-Up Collar    132
9     Peter Pan Collar     96
10      Wingtip Collar     50
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for hemline

hemline Unique valid classes: 4
hemline Pre len: 6
hemline Pre classes:['Curved', 'Straight', 'High-Low', 'Asymmetric', 'Issues_Hemline', 'Image Issue In Hemline']
hemline label_count pre:                     index  hemline
0                  Curved    84099
1                Straight    14314
2                High-Low     1620
3              Asymmetric      111
4          Issues_Hemline       19
5  Image Issue In Hemline        8
hemline Post len: 4
hemline Post classes:['Curved', 'Straight', 'High-Low', 'Asymmetric']
hemline label_count post:         index  hemline
0      Curved    84099
1    Straight    14314
2    High-Low     1620
3  Asymmetric      111
len(data), len(train_rows), len(test_rows)  100144 80115 20029
df_train.shape, df_test.shape (100144, 12) (80115, 6) (20029, 6)
sleeve_length 4 {'Long Sleeves': 0, 'Short Sleeves': 1, 'Three-Quarter Sleeves': 2, 'Sleeveless': 3}
[0 1 3 2]
length 3 {'Regular': 0, 'Crop': 1, 'Longline': 2}
[0 1 2]
print_pattern_type 28 {'Solid': 0, 'Geometric': 1, 'Gingham Checks': 2, 'Micro Ditsy': 3, 'Other Checks': 4, 'Conversational': 5, 'Floral': 6, 'Vertical Stripes': 7, 'Tartan Checks': 8, 'Multi Stripes': 9, 'Textured': 10, 'Polka Dots': 11, 'Faded': 12, 'Micro Checks': 13, 'Bengal Stripes': 14, 'Grid Tattersall Checks': 15, 'Windowpane Checks': 16, 'Abstract': 17, 'Pinstripes': 18, 'Ethnic Motifs': 19, 'Buffalo Checks': 20, 'Colourblocked': 21, 'Horizontal Stripes': 22, 'Graphic': 23, 'Typography': 24, 'Shepherd Checks': 25, 'Camouflage': 26, 'Animal': 27}
[ 3  0 10 12  8 17  2  4 21 13 14  7  5 24 19  1 18 23 20  6 15 11 16 22
  9 26 25 27]
neck 11 {'Spread Collar': 0, 'Cutaway Collar': 1, 'Mandarin Collar': 2, 'Button-Down Collar': 3, 'Slim Collar': 4, 'Collarless': 5, 'Hood': 6, 'Club Collar': 7, 'Built-Up Collar': 8, 'Peter Pan Collar': 9, 'Wingtip Collar': 10}
[ 0  4  2  1  3  5  6  7  8  9 10]
hemline 4 {'Curved': 0, 'Straight': 1, 'High-Low': 2, 'Asymmetric': 3}
[0 2 1 3]

Saving final multilabel file
