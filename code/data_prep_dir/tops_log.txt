pre:(169338, 11)
post:(122584, 11)
data read..
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for sleeve_style

sleeve_style Unique valid classes: 18
sleeve_style Pre len: 21
sleeve_style Pre classes:['Regular Sleeves', 'No Sleeves', 'Cap Sleeves', 'Extended Sleeves', 'Kimono Sleeves', 'Cold-Shoulder Sleeves', 'Cuffed Sleeves', 'Flared Sleeves', 'Roll-Up Sleeves', 'Shoulder Straps', 'Puff Sleeves', 'Raglan Sleeves', 'Bell Sleeves', 'Slit Sleeves', 'Cape Sleeves', 'Flutter Sleeves', 'Batwing Sleeves', 'Bishop Sleeves', 'Image Issue', 'Image_Issue', 'Taxonomy_Issue']
sleeve_style label_count pre:                     index  sleeve_style
0         Regular Sleeves         33322
1              No Sleeves         22433
2          Cuffed Sleeves         17402
3            Bell Sleeves          7631
4             Cap Sleeves          6178
5        Extended Sleeves          5784
6   Cold-Shoulder Sleeves          4424
7          Flared Sleeves          4174
8         Shoulder Straps          4170
9             Image_Issue          3934
10        Roll-Up Sleeves          2766
11        Flutter Sleeves          2526
12         Raglan Sleeves          1572
13         Kimono Sleeves          1430
14           Puff Sleeves          1332
15           Slit Sleeves          1289
16         Bishop Sleeves          1223
17           Cape Sleeves           582
18        Batwing Sleeves           362
19         Taxonomy_Issue            25
20            Image Issue            23
sleeve_style Post len: 18
sleeve_style Post classes:['Regular Sleeves', 'No Sleeves', 'Cap Sleeves', 'Extended Sleeves', 'Kimono Sleeves', 'Cold-Shoulder Sleeves', 'Cuffed Sleeves', 'Flared Sleeves', 'Roll-Up Sleeves', 'Shoulder Straps', 'Puff Sleeves', 'Raglan Sleeves', 'Bell Sleeves', 'Slit Sleeves', 'Cape Sleeves', 'Flutter Sleeves', 'Batwing Sleeves', 'Bishop Sleeves']
sleeve_style label_count post:                     index  sleeve_style
0         Regular Sleeves         33322
1              No Sleeves         22433
2          Cuffed Sleeves         17402
3            Bell Sleeves          7631
4             Cap Sleeves          6178
5        Extended Sleeves          5784
6   Cold-Shoulder Sleeves          4424
7          Flared Sleeves          4174
8         Shoulder Straps          4170
9         Roll-Up Sleeves          2766
10        Flutter Sleeves          2526
11         Raglan Sleeves          1572
12         Kimono Sleeves          1430
13           Puff Sleeves          1332
14           Slit Sleeves          1289
15         Bishop Sleeves          1223
16           Cape Sleeves           582
17        Batwing Sleeves           362
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for sleeve_length

sleeve_length Unique valid classes: 5
sleeve_length Pre len: 8
sleeve_length Pre classes:['Long Sleeves', 'Sleeveless', 'Three-Quarter Sleeves', 'Short Sleeves', 'No Sleeves', 'Image_Issue', 'No Image', 'Image Issue']
sleeve_length label_count pre:                    index  sleeve_length
0          Short Sleeves          34252
1           Long Sleeves          30469
2  Three-Quarter Sleeves          25262
3             Sleeveless          21898
4             No Sleeves           4743
5            Image_Issue           1948
6            Image Issue             20
7               No Image              7
sleeve_length Post len: 5
sleeve_length Post classes:['Long Sleeves', 'Sleeveless', 'Three-Quarter Sleeves', 'Short Sleeves', 'No Sleeves']
sleeve_length label_count post:                    index  sleeve_length
0          Short Sleeves          34252
1           Long Sleeves          30469
2  Three-Quarter Sleeves          25262
3             Sleeveless          21898
4             No Sleeves           4743
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for print_pattern_type

print_pattern_type Unique valid classes: 20
print_pattern_type Pre len: 29
print_pattern_type Pre classes:['Solid', 'Animal', 'Floral', 'Horizontal Stripes', 'Conversational', 'Geometric', 'Vertical Stripes', 'Checked', 'Self Design', 'Ethnic Motifs', 'Graphic', 'Abstract', 'Tie and Dye', 'Typography', 'Tribal', 'Polka Dots', 'Colourblocked', 'Tropical', 'Ombre', 'Embellished', 'Striped', 'Image_Issue', 'Tie And Dye', 'Topography', 'Horizontal Stripe', 'Polka Dot', 'Taxonomy_Issue', 'Other Checks', 'Other']
print_pattern_type label_count pre:                  index  print_pattern_type
0                Solid               50889
1               Floral               17910
2          Self Design                9906
3             Abstract                5773
4     Vertical Stripes                4309
5   Horizontal Stripes                3527
6            Geometric                3312
7        Ethnic Motifs                3013
8       Conversational                2982
9              Graphic                2912
10          Polka Dots                2439
11             Checked                2067
12          Typography                1769
13         Embellished                1694
14       Colourblocked                1195
15              Animal                 893
16            Tropical                 662
17              Tribal                 653
18         Tie and Dye                 301
19         Image_Issue                 164
20               Ombre                 154
21         Tie And Dye                  66
22             Striped                  15
23          Topography                   6
24   Horizontal Stripe                   3
25      Taxonomy_Issue                   3
26               Other                   2
27           Polka Dot                   2
28        Other Checks                   2
print_pattern_type Post len: 20
print_pattern_type Post classes:['Solid', 'Animal', 'Floral', 'Horizontal Stripes', 'Conversational', 'Geometric', 'Vertical Stripes', 'Checked', 'Self Design', 'Ethnic Motifs', 'Graphic', 'Abstract', 'Tie and Dye', 'Typography', 'Tribal', 'Polka Dots', 'Colourblocked', 'Tropical', 'Ombre', 'Embellished']
print_pattern_type label_count post:                  index  print_pattern_type
0                Solid               50889
1               Floral               17910
2          Self Design                9906
3             Abstract                5773
4     Vertical Stripes                4309
5   Horizontal Stripes                3527
6            Geometric                3312
7        Ethnic Motifs                3013
8       Conversational                2982
9              Graphic                2912
10          Polka Dots                2439
11             Checked                2067
12          Typography                1769
13         Embellished                1694
14       Colourblocked                1195
15              Animal                 893
16            Tropical                 662
17              Tribal                 653
18         Tie and Dye                 301
19               Ombre                 154
Filtering the classes having count less than threshold value...
Capping the classes having count greater than threshold value...
Removing invalid classes for neck

neck Unique valid classes: 18
neck Pre len: 24
neck Pre classes:['High Neck', 'Round Neck', 'Scoop Neck', 'Boat Neck', 'V-Neck', 'Keyhole Neck', 'Mandarin Collar', 'Off-Shoulder', 'Shoulder Straps', 'Tie-Up Neck', 'Shirt Collar', 'Cowl Neck', 'One Shoulder', 'Square Neck', 'Halter Neck', 'Strapless', 'Peter Pan Collar', 'Sweetheart Neck', 'Tie- Up Neck', 'Image_Issue', 'Choker', 'Mock Neck', 'Hood', 'Image Is Not Clearly Visible.']
neck label_count pre:                             index   neck
0                      Round Neck  52232
1                          V-Neck  13303
2                 Mandarin Collar   7663
3                    Shirt Collar   6271
4                     Tie-Up Neck   6063
5                    Off-Shoulder   5425
6                 Shoulder Straps   5262
7                      Scoop Neck   5195
8                       High Neck   4798
9                       Boat Neck   4101
10                   Keyhole Neck   2555
11                    Square Neck   1008
12                   One Shoulder    636
13                      Cowl Neck    495
14                    Halter Neck    309
15               Peter Pan Collar    278
16                    Image_Issue    240
17                      Strapless    221
18                         Choker    177
19                Sweetheart Neck    102
20                      Mock Neck     15
21                           Hood      4
22  Image Is Not Clearly Visible.      3
23                   Tie- Up Neck      2
neck Post len: 18
neck Post classes:['High Neck', 'Round Neck', 'Scoop Neck', 'Boat Neck', 'V-Neck', 'Keyhole Neck', 'Mandarin Collar', 'Off-Shoulder', 'Shoulder Straps', 'Tie-Up Neck', 'Shirt Collar', 'Cowl Neck', 'One Shoulder', 'Square Neck', 'Halter Neck', 'Strapless', 'Peter Pan Collar', 'Sweetheart Neck']
neck label_count post:                index   neck
0         Round Neck  52232
1             V-Neck  13303
2    Mandarin Collar   7663
3       Shirt Collar   6271
4        Tie-Up Neck   6063
5       Off-Shoulder   5425
6    Shoulder Straps   5262
7         Scoop Neck   5195
8          High Neck   4798
9          Boat Neck   4101
10      Keyhole Neck   2555
11       Square Neck   1008
12      One Shoulder    636
13         Cowl Neck    495
14       Halter Neck    309
15  Peter Pan Collar    278
16         Strapless    221
17   Sweetheart Neck    102
len(data), len(train_rows), len(test_rows)  115917 92733 23184
/rapid_data/myntra/myntra_data/tops/tops_sleeve_style/tops_sleeve_style_mynt_posh_train.txt
/rapid_data/myntra/myntra_data/tops/tops_sleeve_length/tops_sleeve_length_mynt_posh_train.txt
/rapid_data/myntra/myntra_data/tops/tops_print_pattern_type/tops_print_pattern_type_mynt_posh_train.txt
/rapid_data/myntra/myntra_data/tops/tops_neck/tops_neck_mynt_posh_train.txt
df_train.shape, df_test.shape (115917, 11) (92733, 5) (23184, 5)
sleeve_style 18 {'Regular Sleeves': 0, 'No Sleeves': 1, 'Cap Sleeves': 2, 'Extended Sleeves': 3, 'Kimono Sleeves': 4, 'Cold-Shoulder Sleeves': 5, 'Cuffed Sleeves': 6, 'Flared Sleeves': 7, 'Roll-Up Sleeves': 8, 'Shoulder Straps': 9, 'Puff Sleeves': 10, 'Raglan Sleeves': 11, 'Bell Sleeves': 12, 'Slit Sleeves': 13, 'Cape Sleeves': 14, 'Flutter Sleeves': 15, 'Batwing Sleeves': 16, 'Bishop Sleeves': 17}
[ 1  5  0  7 10  3 12 13  8  2 11  6  9  4 15 16 17 14]
sleeve_length 5 {'Long Sleeves': 0, 'Sleeveless': 1, 'Three-Quarter Sleeves': 2, 'Short Sleeves': 3, 'No Sleeves': 4}
[1 2 3 0 4]
print_pattern_type 20 {'Solid': 0, 'Animal': 1, 'Floral': 2, 'Horizontal Stripes': 3, 'Conversational': 4, 'Geometric': 5, 'Vertical Stripes': 6, 'Checked': 7, 'Self Design': 8, 'Ethnic Motifs': 9, 'Graphic': 10, 'Abstract': 11, 'Tie and Dye': 12, 'Typography': 13, 'Tribal': 14, 'Polka Dots': 15, 'Colourblocked': 16, 'Tropical': 17, 'Ombre': 18, 'Embellished': 19}
[ 2  4  0  5  8  9 16 11  6 13  3 17 12 10 15 14 18 19  7  1]
neck 18 {'High Neck': 0, 'Round Neck': 1, 'Scoop Neck': 2, 'Boat Neck': 3, 'V-Neck': 4, 'Keyhole Neck': 5, 'Mandarin Collar': 6, 'Off-Shoulder': 7, 'Shoulder Straps': 8, 'Tie-Up Neck': 9, 'Shirt Collar': 10, 'Cowl Neck': 11, 'One Shoulder': 12, 'Square Neck': 13, 'Halter Neck': 14, 'Strapless': 15, 'Peter Pan Collar': 16, 'Sweetheart Neck': 17}
[ 2  1  4  9  5  6  0  8 11  3 12 14  7 10 13 17 15 16]

Saving final multilabel file
