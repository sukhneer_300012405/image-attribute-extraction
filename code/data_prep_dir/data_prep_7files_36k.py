import os
import glob
import pandas as pd
import numpy as np
import argparse
import json
import random

article_type = 'dresses'
attributes = ['sleeve_style','sleeve_length','length','print_pattern_type','neck','hemline', 'shape']
output_path = '/rapid_data/myntra/myntra_data/' + article_type
train_file = '/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/multilabel_train.txt'
test_file = '/rapid_data/myntra/myntra_data/' + article_type + '/multi_label/multilabel_test.txt'

train_df = pd.read_csv(train_file, sep = ' ', header = None )
test_df = pd.read_csv(test_file, sep =' ', header = None)

train_df.columns = ['local_path'] + attributes
test_df.columns = ['local_path'] + attributes

for attribute in attributes:
    temp_train = train_df[['local_path', attribute]]
#     temp_train = temp_train.sample(frac=1)
    temp_test = test_df[['local_path', attribute]]
#     temp_test = temp_test.sample(frac=1)
    path = os.path.join(output_path, article_type + '_' + attribute)
#     print( path + '/' + article_type + '_' + attribute + '_new_train.txt')
    temp_train.to_csv(path +  '/' + article_type + '_' + attribute + '_new_train.txt', sep=' ', index = False, header = False)
    temp_test.to_csv(path + '/' + article_type + '_' + attribute + '_new_test.txt', sep=' ', index = False, header = False)