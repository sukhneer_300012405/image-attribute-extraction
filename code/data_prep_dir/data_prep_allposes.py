import os
import glob
import pandas as pd
import numpy as np
import argparse
import json

article_type = 'dresses'
parser = argparse.ArgumentParser()
parser.add_argument('--input_file_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '_all_poses' + '/' + article_type +  '_all_poses_valid_styles.csv')
parser.add_argument('--image_path', default = '/rapid_data/myntra/myntra_data/' + article_type + '_all_poses' + '/' + article_type +  '_all_poses_images')
parser.add_argument('--attribute_map_file', default= '/rapid_data/myntra/myntra_data/' + article_type + '_all_poses' +  '/' + article_type + '_' + 'attribute_map.txt' ,type=str)
parser.add_argument('--attributes', default =['sleeve_style', 'sleeve_length','length', 'print_pattern_type', 'neck','hemline', 'shape'], type = str)
parser.add_argument('--output_path', default= '/rapid_data/myntra/myntra_data/' + article_type + '_all_poses'  ,type=str)
# parser.add_argument('--label_map_file_name', default = None, type = str)
parser.add_argument('--overall_map_file_name', default = article_type +  "_overall_map_file.csv" ,type = str)
parser.add_argument('--split_ratio', default = 0.8 ,type = float)
parser.add_argument('--ignore_threshhold', default = 500 ,type = int)
parser.add_argument('--train_file_suffix', default = "train",type = str)
parser.add_argument('--test_file_suffix', default = "test", type = str)

def main(args):
    data = pd.read_csv(args.input_file_path)
    data.columns = ['image_url', 'style_id', 'style_id_appended', 'brand', 'article_type','gender', 'base_colour',  'sleeve_style', 'sleeve_length', 'length', 'print_pattern_type', 'neck', 'hemline', 'shape']
    data['style_id'] = data['style_id_appended']
    del data['style_id_appended']

    print("data read..")
    if args.attribute_map_file:
        mappers = {}
        with open(args.attribute_map_file) as json_data:
            print("reading map file...")
            mappers = json.load(json_data)
            data = data.replace(mappers)
            
    data["local_image_path"] = args.image_path + "/" + data["style_id"].map(str) + ".jpg"
    data.to_csv(os.path.join(args.output_path, args.overall_map_file_name))
    print("overall csv map file created")
    
    attributes = args.attributes
    ignore_threshhold = args.ignore_threshhold
    print ('\nattributes:\n' + str(attributes))   
    for attribute in attributes:
        directory = os.path.join(args.output_path, article_type + '_' + attribute)
        if not os.path.exists(directory):
            print("creating directory for attribute :\n"+ attribute)
            os.makedirs(directory)

        overall_df = data.copy()  
        overall_df = overall_df[~overall_df[attribute].isnull()]
        overall_df = overall_df[['style_id', 'image_url', 'local_image_path', attribute]]
        print ('\n' + attribute + ' dataframe columns:\n'  + str(overall_df.columns) )
        
        print ('Filtering the classes having count less than threshold value...')
        label_count = pd.DataFrame(overall_df[attribute].value_counts()).reset_index()
        label_count = label_count[label_count[attribute] > ignore_threshhold]
                
        unique_val = overall_df[attribute].unique().tolist()
        print  ('\nUnique ' + attribute + ' values before removing classes with very less count:' +  str(unique_val) )
        
        overall_df = overall_df[ overall_df[attribute].isin(label_count['index'])]
        
        unique_val = overall_df[attribute].unique().tolist()
        print  ('\nUnique ' + attribute + ' Values after removing classes with very less count:' +  str(unique_val) )

        labels_dict = {i:j for j,i in enumerate(unique_val)}
        print ('\n' + str(labels_dict))
        print ('\nWriting label map file')
        with open(directory + '/' + article_type + '_' + attribute + '_label_map.txt', 'w') as file:
            file.write(json.dumps(labels_dict))

        df_train = pd.DataFrame()
        df_test = pd.DataFrame()
        split = args.split_ratio
        
        print ('\nSplitting data into train and test...')
        for val in unique_val:
            temp = overall_df[overall_df[attribute] == val].reset_index(drop = True)
            train_rows = np.random.choice(temp.index, int(len(temp)* split), replace=False)
            test_rows = [x for x in temp.index if x not in train_rows]
            temp_train = temp.loc[train_rows]
            temp_test = temp.loc[test_rows]
            df_train = df_train.append(temp_train)
            df_test = df_test.append(temp_test) 
            
        print ('\nSaving train reference and test reference file for' + attribute )
        df_train.to_csv(directory + '/' + article_type + '_' + attribute + '_' + args.train_file_suffix + '_ref.csv')
        df_test.to_csv(directory + '/' + article_type + '_' + attribute + '_' + args.test_file_suffix  + '_ref.csv')
        
        df_train[attribute] = df_train[attribute].map(labels_dict)
        df_test[attribute] = df_test[attribute].map(labels_dict)
        df_train = df_train[['local_image_path', attribute]]
        df_test = df_test[['local_image_path', attribute]]
                
        print  ('\nSaving final train and test file for' + attribute  )
        df_train.to_csv(directory + '/' + article_type + '_' + attribute+ '_' + args.train_file_suffix + '.txt', sep=' ', index = False, header=False)
        df_test.to_csv(directory + '/' + article_type + '_' + attribute+ '_' + args.test_file_suffix  + '.txt', sep=' ', index = False, header=False)
    
if __name__ == '__main__':
    args = parser.parse_args()
    main(args)